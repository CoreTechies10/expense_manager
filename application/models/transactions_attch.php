<?php /**
 * Created by PhpStorm.
 * User: Hitesh balyan(coolbalyan)
 * Date: 16/03/16
 * Time: 16:12
 */

/**
 * Class Transactions_Attch
 * @property CI_DB_active_record db
 */
class Transactions_Attch extends CI_Model {

  protected static $table = 'transactions_attch';


  public function getTable() {
    return self::$table;
  }
}