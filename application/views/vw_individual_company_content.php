<?php
if(isset($error) && $error){
  ?>
  <style>
    .display_data{display: none}
    .display_input_data{display: block}
  </style>
  <?php
}else{
  ?>
  <style>
    .display_data{display: block}
    .display_input_data{display: none}
  </style>
  <?php
}
?>
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <!-- show success message -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable" style="margin-top: 20px">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif; ?>

      <!-- show error message -->
      <?php if (isset($error)): // $this->session->flashdata('error')?>
        <div class="alert alert-danger alert-dismissable" style="margin-top: 20px">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo  $error; //$this->session->flashdata('error'); ?>
        </div>
      <?php endif; ?>
      <!-- End show success message -->

      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan"
                style="margin-bottom:0px; line-height:1.7em;"><?= $supplier[ 'supplier_name' ] ?></h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span
                      class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>

              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile">
            <h3 class="txt-blue"> Profile</h3>
            <form method="post">
              <table class="table table-bordered">
                <tr>
                  <td>Comany Name</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'supplier_name' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Supplier Name" type="text" name="supplier_name" value="<?php if(set_value('supplier_name')) echo set_value('supplier_name'); else echo $supplier[ 'supplier_name' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('supplier_name') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Contact First Name</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'supplier_f_name' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="First Name" type="text" name="supplier_f_name" value="<?php if(set_value('supplier_f_name')) echo set_value('supplier_f_name'); else echo $supplier[ 'supplier_f_name' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('supplier_f_name') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Contact Last Name</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'supplier_l_name' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Last Name" type="text" name="supplier_l_name" value="<?php if(set_value('supplier_l_name')) echo set_value('supplier_l_name'); else echo $supplier[ 'supplier_l_name' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('supplier_l_name') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'address' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Address" type="text" name="address" value="<?php if(set_value('address')) echo set_value('address'); else echo $supplier[ 'address' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('address') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Country</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'country' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Country" type="text" name="country" value="<?php if(set_value('country')) echo set_value('country'); else echo $supplier[ 'country' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('country') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>City</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'city' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="City" type="text" name="city" value="<?php if(set_value('city')) echo set_value('city'); else echo $supplier[ 'city' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('city') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>

                    <span class="display_data"><?= $supplier[ 'email' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Email" type="text" name="email" value="<?php if(set_value('email')) echo set_value('email'); else echo $supplier[ 'email' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('email') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Tel No.</td>
                  <td>
                    <span class="display_data"><?= $supplier[ 'phone' ] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Phone" type="text" name="phone" value="<?php if(set_value('phone')) echo set_value('phone'); else echo $supplier[ 'phone' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('phone') ?></span>
                    </div>
                    </td>

                </tr>
                <tr>
                  <td>Website Address</td>
                  <td>

                    <span class="display_data"><a href="<?= $supplier[ 'website' ] ?>" target="_blank"><?= $supplier[ 'website' ] ?></a></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Website" type="text" name="website" value="<?php if(set_value('website')) echo set_value('website'); else echo $supplier[ 'website' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('website') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Twitter ID</td>
                  <td>

                    <span class="display_data"><a href="https://twitter.com/<?= $supplier[ 'twitter_id' ] ?>" target="_blank"><?= $supplier[ 'twitter_id' ] ?></a></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Twitter ID" type="text" name="twitter_id" value="<?php if(set_value('twitter_id')) echo set_value('twitter_id'); else echo $supplier[ 'twitter_id' ]; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('twitter_id') ?></span>
                    </div>
                  </td>
                </tr>

              </table>

              <div class="col-md-2 pull-right text-right">
                <a id="edit_user" class="display_data btn btn-xs btn-info" ><i class="fa fa-edit"></i> Edit</a><!-- onclick="MyWindow=window.open('<?php echo base_url() ?>user/edit','update','scrollbars=yes,width=500,height=400,menubar=0,toolbar=0,location=0,directories=0,status=0,copyhistory=0'); return false;" -->
                <button name="edit_user" class="display_input_data btn btn-xs btn-info" ><i class="fa fa-edit"></i> Edit</button><!-- onclick="MyWindow=window.open('<?php echo base_url() ?>user/edit','update','scrollbars=yes,width=500,height=400,menubar=0,toolbar=0,location=0,directories=0,status=0,copyhistory=0'); return false;" -->

              </div>
            </form>

          </div>
        </div>

        <div class="col-lg-6">
          <div class="exp-company-profile" style="min-height:66vh;">
            <div>
              <div class="col-lg-6 null-padding">
                <h3 class="txt-cyan">Suppliers</h3>
              </div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
              <tr class="bg-blue bg-gray">
                <th>Company</th>
                <th>Firstname</th>
                <th>Details</th>
              </tr>
              </thead>
              <tbody>
              <?php
              foreach ($all_supplier as $local_supplier) {
                echo "<tr>
                      <td>{$local_supplier['supplier_name']}</td>
                      <td>{$local_supplier['supplier_f_name']}</td>
                      <td> <a href='" . base_url('supplier/view/' . $local_supplier[ 'ID' ]) . "'>Details</a></td>
                    </tr>";

              }

              ?>
              </tbody>

            </table>
          </div>
        </div>

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table Calander-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span
                class="exp-title-1 txt-blue"> Delivery Value from <?= $supplier[ 'supplier_name' ] ?></span>
            </h3>
          </div>
          <div class="col-lg-5 text-right exp-delevery">
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row
            </button>
            <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column
            </button>
            <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
            <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete
            </button>

          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered exp-data-table" id="delivery-table">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Pay Type</th>
            <th>Desc</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>

          <?php
          foreach ($supplier_t as $details) {
            if ($details[ 'is_delivered' ] == 'yes') {
              echo "<tr>
                      <td>{$details['datetime']}</td>
                      <td>{$details['supplier_name']}</td>
                      <td>{$details['supplier_f_name']}</td>
                      <td>{$details['name']}</td>
                      <td>{$details['meeting_categories'][0]['value']}</td>
                      <td>{$details['transaction_categories'][1]['value']}</td>
                      <td>{$details['description']}</td>
                      <td>{$details['amount']}</td>
                    </tr>";
            }
          }

          ?>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <div class="col-lg-7">
          <h3><i class="fa fa-money"></i> <span class="exp-title-1 txt-blue">Amount Paid in Advance to AbcFood Williams (Will)</span>
          </h3>
        </div>
        <div class="col-lg-5 text-right exp-amount">
          <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row
          </button>
          <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column
          </button>
          <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
          <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete
          </button>

        </div>
        <div class="clearfix"></div>
        <div class="clearfix"></div>
        <table class="table dataTable table-bordered">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Pay Type</th>
            <th>Desc</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <?php
          foreach ($supplier_t as $details) {
            if ($details[ 'is_advance' ] == 'yes') {
              echo "<tr>
                      <td>{$details['datetime']}</td>
                      <td>{$details['supplier_name']}</td>
                      <td>{$details['supplier_f_name']}</td>
                      <td>{$details['name']}</td>
                      <td>{$details['meeting_categories'][0]['value']}</td>
                      <td>{$details['transaction_categories'][1]['value']}</td>
                      <td>{$details['description']}</td>
                      <td>{$details['amount']}</td>
                    </tr>";
            }
          }

          ?>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <div class="col-lg-7">
          <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">UpComming Meetings</span></h3>
        </div>
        <div class="col-lg-5 text-right exp-upcomming">
          <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row
          </button>
          <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column
          </button>
          <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
          <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete
          </button>

        </div>
        <div class="clearfix"></div>
        <table class="table dataTable table-bordered">
          <thead>
          <tr class="bg-blue bg-gray">
            <th>Date</th>
            <th>Company Name</th>
            <th>Contact FnLname</th>
            <th>Meeting</th>
            <th>Catogry</th>
            <th>Payment</th>
            <th>Desc</th>
            <th>Amount</th>
          </tr>
          </thead>
          <tbody>
          <?php
          ?>
          <?php
          foreach ($up_meeting as $m_details) {

            echo "<tr>
                    <td>{$m_details['m_datetime']}</td>
                    <td>{$m_details['supplier_name']}</td>
                    <td>{$m_details['supplier_f_name']}</td>
                    <td>{$m_details['name']}</td>
                    <td>{$m_details['meeting_categories'][0]['value']}</td>
                    <td>{$m_details['transaction_categories'][1]['value']}</td>
                    <td>{$m_details['description']}</td>
                    <td>{$m_details['amount']}</td>
                  </tr>";

          }

          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container" style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi
                vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span> </a></div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo.
                Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi
                vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav><span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span>
              </nav>
              <h2 id="custom-month" class="custom-month"></h2>

              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>