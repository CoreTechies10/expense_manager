<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile "
           style="margin-top:20px; margin-bottom:20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">
              Add New/Edit </h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar"
                   style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button"
                          class="btn btn-default dropdown-toggle btn-xs btn-block"
                          data-toggle="dropdown" aria-haspopup="true"
                          aria-expanded="false"><i class="fa fa-bars"></i> <span
                      class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>-->
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- Table Calander-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <!-- show success message -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable">

          <button type="button" class="close" data-dismiss="alert"
                  aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif; ?>

      <!-- show error message -->
      <?php if ($this->session->flashdata('error')): ?>
        <div class="alert alert-danger alert-dismissable">

          <button type="button" class="close" data-dismiss="alert"
                  aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('error'); ?>
        </div>
      <?php endif; ?>

      <div class="row">
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>

              <div class="col-lg-7">
                <h4><span
                    class="exp-title-1 txt-blue">Add/Edit Meeting Type</span>
                </h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>


              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Meeting Type</th>
                <th>Edit/Delete</th>

              </tr>
              </thead>
              <thead>
              <form method="post">
                <td>
                  <input type="text" name="meeting_type_category" maxlength="50"
                         value="<?php echo set_value('meeting_type_category'); ?>"/>
                  <span
                    class="error"><?php echo form_error('meeting_type_category'); ?></span>
                </td>
                <td>
                  <button type="submit" name="add_meeting_category"
                          class="btn btn-xs btn-success"><i class="fa fa-save">
                      Save</i></button>
                </td>
              </form>
              </thead>
              <tbody>

              <?php
              $categories = get_category_list(NULL, 1, $this->login_user['id']);
              if ($categories->num_rows > 0):
                $categories = $categories->result_array();
                foreach ($categories as $category):
                  if (isset($_POST[ 'category_id' ]) && $_POST[ 'category_id' ] == $category[ 'ID' ]) {
                    $input_display = 'inline-block';
                    $value_display = 'none';
                  }
                  else {
                    $input_display = 'none';
                    $value_display = 'inline-block';
                  }
                  ?>

                  <tr>
                    <form method="post">
                      <td>
                        <span id="value_<?php echo $category[ 'ID' ]; ?>"
                              style="display: <?php echo $value_display ?>"><?php echo $category[ 'value' ]; ?></span>

                        <div id="input_<?php echo $category[ 'ID' ]; ?>"
                             style="display: <?php echo $input_display ?>">
                          <input type="text" maxlength="50" name="value"
                                 value="<?php echo $category[ 'value' ]; ?>">
                          <span
                            class="error"><?php echo form_error('value'); ?></span>
                          <input type="hidden" name="category_id"
                                 value="<?php echo $category[ 'ID' ]; ?>">
                        </div>
                      </td>

                      <td>
                        <button type="button"
                                id="<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-info edit_category"
                                title="edit"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" name="delete_category"
                                id="delete_<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-danger" title="delete"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-trash-o"></i></button>
                        <button type="submit" name="edit_category"
                                id="submit_<?php echo $category[ 'ID' ]; ?>"
                                title="edit"
                                style="display: <?php echo $input_display ?>"
                                class="btn btn-xs btn-info"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" class="btn btn-xs btn-danger"
                                name="cancel_category"
                                id="cancel_<?php echo $category[ 'ID' ]; ?>"
                                title="cancel"
                                style="display: <?php echo $input_display ?>"><i
                            class="fa fa-times"></i></button>
                      </td>
                    </form>

                  </tr>

                  <?php
                endforeach;
              endif;
              ?>

              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>
              <div class="col-lg-7">
                <h4><span
                    class="exp-title-1 txt-blue">Add/Edit Meeting Location</span>
                </h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>
              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Meeting Location</th>
                <th>Add/Edit/Delete</th>

              </tr>

              </thead>
              <thead>
              <form method="post">
                <td>
                  <input type="text" name="meeting_type_location" maxlength="50"
                         value="<?php echo set_value('meeting_type_location'); ?>"/>
                  <span
                    class="error"><?php echo form_error('meeting_type_location'); ?></span>
                </td>
                <td>
                  <button type="submit" name="add_meeting_location"
                          class="btn btn-xs btn-success"><i class="fa fa-save">
                      Save</i></button>
                </td>
              </form>
              </thead>
              <tbody>
              <?php
              $categories = get_category_list(NULL, 2, $this->login_user['id']);
              if ($categories->num_rows > 0):
                $categories = $categories->result_array();
                foreach ($categories as $category):
                  if (isset($_POST[ 'category_id' ]) && $_POST[ 'category_id' ] == $category[ 'ID' ]) {
                    $input_display = 'inline-block';
                    $value_display = 'none';
                  }
                  else {
                    $input_display = 'none';
                    $value_display = 'inline-block';
                  }
                  ?>

                  <tr>
                    <form method="post">
                      <td>
                        <span id="value_<?php echo $category[ 'ID' ]; ?>"
                              style="display: <?php echo $value_display ?>"><?php echo $category[ 'value' ]; ?></span>

                        <div id="input_<?php echo $category[ 'ID' ]; ?>"
                             style="display: <?php echo $input_display ?>">
                          <input type="text" maxlength="50" name="value"
                                 value="<?php echo $category[ 'value' ]; ?>">
                          <span
                            class="error"><?php echo form_error('value'); ?></span>
                          <input type="hidden" name="category_id"
                                 value="<?php echo $category[ 'ID' ]; ?>">
                        </div>
                      </td>

                      <td>
                        <button type="button"
                                id="<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-info edit_category"
                                title="edit"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" name="delete_category"
                                id="delete_<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-danger" title="delete"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-trash-o"></i></button>
                        <button type="submit" name="edit_category"
                                id="submit_<?php echo $category[ 'ID' ]; ?>"
                                title="edit"
                                style="display: <?php echo $input_display ?>"
                                class="btn btn-xs btn-info"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" class="btn btn-xs btn-danger"
                                name="cancel_category"
                                id="cancel_<?php echo $category[ 'ID' ]; ?>"
                                title="cancel"
                                style="display: <?php echo $input_display ?>"><i
                            class="fa fa-times"></i></button>
                      </td>
                    </form>

                  </tr>

                  <?php
                endforeach;
              endif;
              ?>


              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>

      </div>

      <div class="row"
           style="border-top:1px solid #d7d7d7; padding-top:30px; margin-top:30px;">
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>

              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Catagory</span>
                </h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>


              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Category Type</th>
                <th>Edit/Delete</th>

              </tr>
              </thead>
              <thead>
              <form method="post">
                <td>
                  <input type="text" name="transaction_category" maxlength="50"
                         value="<?php echo set_value('transaction_category'); ?>"/>
                  <span
                    class="error"><?php echo form_error('transaction_category'); ?></span>
                </td>
                <td>
                  <button type="submit" name="add_transaction_category"
                          class="btn btn-xs btn-success"><i class="fa fa-save">
                      Save</i></button>
                </td>
              </form>
              </thead>
              <tbody>
              <?php
              $categories = get_category_list(NULL, 3, $this->login_user['id']);
              if ($categories->num_rows > 0):
                $categories = $categories->result_array();
                foreach ($categories as $category):
                  if (isset($_POST[ 'category_id' ]) && $_POST[ 'category_id' ] == $category[ 'ID' ]) {
                    $input_display = 'inline-block';
                    $value_display = 'none';
                  }
                  else {
                    $input_display = 'none';
                    $value_display = 'inline-block';
                  }
                  ?>

                  <tr>
                    <form method="post">
                      <td>
                        <span id="value_<?php echo $category[ 'ID' ]; ?>"
                              style="display: <?php echo $value_display ?>"><?php echo $category[ 'value' ]; ?></span>

                        <div id="input_<?php echo $category[ 'ID' ]; ?>"
                             style="display: <?php echo $input_display ?>">
                          <input type="text" maxlength="50" name="value"
                                 value="<?php echo $category[ 'value' ]; ?>">
                          <span
                            class="error"><?php echo form_error('value'); ?></span>
                          <input type="hidden" name="category_id"
                                 value="<?php echo $category[ 'ID' ]; ?>">
                        </div>
                      </td>

                      <td>
                        <button type="button"
                                id="<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-info edit_category"
                                title="edit"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" name="delete_category"
                                id="delete_<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-danger" title="delete"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-trash-o"></i></button>
                        <button type="submit" name="edit_category"
                                id="submit_<?php echo $category[ 'ID' ]; ?>"
                                title="edit"
                                style="display: <?php echo $input_display ?>"
                                class="btn btn-xs btn-info"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" class="btn btn-xs btn-danger"
                                name="cancel_category"
                                id="cancel_<?php echo $category[ 'ID' ]; ?>"
                                title="cancel"
                                style="display: <?php echo $input_display ?>"><i
                            class="fa fa-times"></i></button>
                      </td>
                    </form>

                  </tr>

                  <?php
                endforeach;
              endif;
              ?>

              </tbody>
            </table>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-tables padding-top0">
            <header>
              <div class="col-lg-7">
                <h4><span class="exp-title-1 txt-blue">Add/Edit Payment </span>
                </h4>
              </div>
              <div class="col-lg-5 text-right">

              </div>
              <div class="clearfix"></div>
            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Payment Type</th>
                <th>Add/Edit/Delete</th>

              </tr>

              </thead>
              <thead>
              <form method="post">
                <td>
                  <input type="text" name="transaction_payment" maxlength="50"
                         value="<?php echo set_value('transaction_payment'); ?>"/>
                  <span
                    class="error"><?php echo form_error('transaction_payment'); ?></span>
                </td>
                <td>
                  <button type="submit" name="add_transaction_payment"
                          class="btn btn-xs btn-success"><i class="fa fa-save">
                      Save</i></button>
                </td>
              </form>
              </thead>
              <tbody>
              <?php
              $categories = get_category_list(NULL, 4, $this->login_user['id']);
              if ($categories->num_rows > 0):
                $categories = $categories->result_array();
                foreach ($categories as $category):
                  if (isset($_POST[ 'category_id' ]) && $_POST[ 'category_id' ] == $category[ 'ID' ]) {
                    $input_display = 'inline-block';
                    $value_display = 'none';
                  }
                  else {
                    $input_display = 'none';
                    $value_display = 'inline-block';
                  }
                  ?>

                  <tr>
                    <form method="post">
                      <td>
                        <span id="value_<?php echo $category[ 'ID' ]; ?>"
                              style="display: <?php echo $value_display ?>"><?php echo $category[ 'value' ]; ?></span>

                        <div id="input_<?php echo $category[ 'ID' ]; ?>"
                             style="display: <?php echo $input_display ?>">
                          <input type="text" maxlength="50" name="value"
                                 value="<?php echo $category[ 'value' ]; ?>">
                          <span
                            class="error"><?php echo form_error('value'); ?></span>
                          <input type="hidden" name="category_id"
                                 value="<?php echo $category[ 'ID' ]; ?>">
                        </div>
                      </td>

                      <td>
                        <button type="button"
                                id="<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-info edit_category"
                                title="edit"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" name="delete_category"
                                id="delete_<?php echo $category[ 'ID' ]; ?>"
                                class="btn btn-xs btn-danger" title="delete"
                                style="display: <?php echo $value_display ?>"><i
                            class="fa fa-trash-o"></i></button>
                        <button type="submit" name="edit_category"
                                id="submit_<?php echo $category[ 'ID' ]; ?>"
                                title="edit"
                                style="display: <?php echo $input_display ?>"
                                class="btn btn-xs btn-info"><i
                            class="fa fa-edit"></i></button>
                        <button type="submit" class="btn btn-xs btn-danger"
                                name="cancel_category"
                                id="cancel_<?php echo $category[ 'ID' ]; ?>"
                                title="cancel"
                                style="display: <?php echo $input_display ?>"><i
                            class="fa fa-times"></i></button>
                      </td>
                    </form>

                  </tr>

                  <?php
                endforeach;
              endif;
              ?>

              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>

      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"
     style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span>
              </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i
                  class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                scelerisque ante sollicitudin commodo. Cras purus odio,
                vestibulum in vulputate at, tempus viverra turpis. Fusce
                condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span>
              </a></div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i
                  class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit
                amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"><a href="#"> 16 <span>Sep</span>
              </a></div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i
                  class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus
                scelerisque ante sollicitudin commodo. Cras purus odio,
                vestibulum in vulputate at, tempus viverra turpis. Fusce
                condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav><span id="custom-prev" class="custom-prev"></span> <span
                  id="custom-next" class="custom-next"></span></nav>
              <h2 id="custom-month" class="custom-month"></h2>

              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>