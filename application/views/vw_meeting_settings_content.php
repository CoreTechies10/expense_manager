<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em; font-family: 'OpenSans-Semibold'"><i class="fa fa-wrench"></i>  Meeting Settings</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>--> 
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>



<!-- Table Calander-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
    <div class="row">
      <div class="col-lg-6">
      		<div class="exp-tables padding-top0">
        <header>
         
         	 <div class="col-lg-7">
                     <h3><i class="fa fa-plus-circle"></i>  <span class="exp-title-1 txt-blue">Add/Edit Meeting Type</span></h3>
          </div>
          <div class="col-lg-5 text-right"> 
            	<button type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"> Add</i></button>
          </div>
         
          
          <div class="clearfix"></div>
        </header>
        <table class="table table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Meeting Type</th>
              <th>Edit/Delete</th>
             
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>New Project</td>
              <td>
              		<button type="button" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
              </td>
              
            </tr>
            <tr>
              <td>Conference</td>
              <td>
              		<button type="button" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
              </td>
              
            </tr>
            
          </tbody>
        </table>
      </div>
      </div>
      <div class="col-lg-6">
      		<div class="exp-tables padding-top0">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-plus-circle"></i>  <span class="exp-title-1 txt-blue">Add/Edit Meeting Location</span></h3>
          </div>
          <div class="col-lg-5 text-right"> 
            	<button type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"> Add</i></button>
          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Meeting Location</th>
              <th>Add/Edit/Delete</th>
             
            </tr>
           
          </thead>
          <tbody>
          	
            <tr>
              <td>Mariot NC</td>
              <td>
              		<button type="button" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
              </td>
              
            </tr>
            <tr>
              <td>HIlton BC</td>
              <td>
              		<button type="button" class="btn btn-xs btn-info"><i class="fa fa-edit"></i></button>
                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash-o"></i></button>
              </td>
              
            </tr>
            
          </tbody>
        </table>
      </div>
      </div>
      <div class="clearfix"></div>
      
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"  style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 event-section">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav> <span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span> </nav>
              <h2 id="custom-month" class="custom-month"></h2>
              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>