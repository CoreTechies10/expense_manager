<?php

/**
 * Created by PhpStorm.
 * User: Hitesh Balyan
 * Date: 04-03-2016
 * Time: 12:00 AM
 */

/**
 * Class Meeting
 * @property CI_DB_active_record db
 */
class MeetingModel extends CI_Model {

    protected static $table = 'meetings';
    protected static $sub_tables = array();

    public function __construct()
    {
        parent::__construct();
        //setting sub table
        $this->load->model('categories');
        self::$sub_tables['category_type'] = $this->categories->getTable();
        self::$sub_tables['meeting_category'] = 'meeting_category';
        self::$sub_tables['category_data'] = 'category_data';
    }

    public function getTable()
    {
        return self::$table;
    }

  public function getAll($userId = '') {
    if (empty($userId)) {
      $userId = $this->login_user['id'];
      if ($userId === FALSE) {
        return FALSE;
      }
    }

        $result = $this->db->get_where(self::$table, array('user_id_fk' => $userId));
        //@var CI_DB_mysql_result $result ;
        $result = $result->result_array();

    if (!empty($result)) {
      return $result;
    }
    return FALSE;
  }

    public function getSubTables($table = '')
    {
        return empty($table) ? self::$sub_tables : self::$sub_tables[$table];
    }

    public function getOne($userId = '', $meeting_id = NULL)
    {
        if (empty($userId)) {
            $this->load->library('session');
            $userId = $this->session->userdata('ID');
            if ($userId === FALSE) {
                return [];
            }
        }
        $where = array('user_id_fk' => $userId);
        if (!empty($meeting_id)) {
            $where['ID'] = $meeting_id;
        }
        $result = $this->db->get_where(self::$table, $where, 1);
        //@var CI_DB_mysql_result $result ;
        $result = $result->result();

        if (!empty($result)) {
            return $result[0];
        }
        return [];
    }

    public function update($data)
    {
        return $this->db->update(self::$table, $data);
    }

    public function insert(
        $data,
        $categories,
        $transactions = [],
        $transactions_categories = [],
        $transactions_attachments = []
    )
    {
        $this->db->insert(self::$table, $data);
        $meetingId = $this->db->insert_id();
        $tmp_category = array();
        foreach ($categories as $category) {
            $tmp_category[] = array('category_data_id_fk' => $category, 'meeting_id_fk' => $meetingId);
        }
        $categories = $tmp_category;

        $categoriesId = $this->db->insert_batch($this->getSubTables('meeting_category'), $categories);

        if (!empty($transactions)) {
            $this->load->model('transactionsmodel');
            $tmp_transaction = array();
            foreach ($transactions as $transaction) {
                $transaction['meeting_id_fk'] = $meetingId;
                $tmp_transaction[] = $transaction;
            }
            $this->transactionsmodel->addTransaction($tmp_transaction, $transactions_categories,
                $transactions_attachments);
        }
    }

  public function getUpcoming($userId = '', $supplier_id = NULL) {
    if (empty($userId)) {
      $userId = $this->login_user['id'];
      if ($userId === FALSE) {
        return [];
      }
    }

    $where = array('m.user_id_fk' => $userId, 'm.datetime >' => date('Y-m-d'));
    if (!empty($meeting_id)) {
      $where[ 'm.ID' ] = $meeting_id;
    }

    if (!empty($supplier_id)) {
      $this->load->model('transactionsmodel');
      $this->load->model('suppliers');
      $this->db->join($this->transactionsmodel->getTable() . ' t', 'm.ID = t.meeting_id_fk');
      $this->db->join($this->suppliers->getTable() . ' s', 's.ID=t.supplier_id_fk');
      $where[ 't.supplier_id_fk' ] = $supplier_id;
    }
    $this->db->select('m.datetime m_datetime, m.*,s.*, t.*');

    $this->db->group_by('m.ID');
    $result = $this->db->get_where(self::$table . ' m', $where);
    $this->load->model('categories_data');

    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();

    if (!empty($result)) {
      foreach ($result as $key => $meeting) {
        $tmp_meeting_result = $this->categories_data->getMeetingCategory($userId, $meeting[ 'meeting_id_fk' ]);

        $tmp_transaction_result = $this->categories_data->getTransactionCategory($userId, $meeting[ 'meeting_id_fk' ],
          $meeting[ 'supplier_id_fk' ]);
        $result[ $key ] += array(
          'meeting_categories' => $tmp_meeting_result,
          'transaction_categories' =>
            $tmp_transaction_result
        );
      }

      return $result;
    }
    return [];
  }

}