<?php if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}

/**
 * Class Supplier
 * @property CI_DB_active_record $db
 * @property CI_Loader $load
 * @property Suppliers $suppliers
 * @property CI_Form_validation $form_validation
 * @property CI_Session $session
 */
class Supplier extends MY_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->model('suppliers');
    //check for user login

    if(!$this->login_user['id'] && !check_login_status($this->login_user['id'])){
      redirect('home');
    }
  }

  public function index($supplier_id = NULL) {
    $all_suppliers = $this->suppliers->getAll(1);
    $this->load->view('vw_supplier_profile');
  }

  /**
   * Add supplier
   */
  public function add($supplier_id = NULL) {
    $error = FALSE;


    if (isset($_POST[ 'supplier_add' ])) {
      foreach ($_POST as $field => $i) {
        $this->formValidationRules($field);
      }
      if ($this->form_validation->run() == FALSE) {

        $data['error'] = validation_errors();
        //$this->session->set_flashdata('error', '<span ></span>');
        //redirect(base_url('supplier/add'));
      }
      else {


        $userId = $this->login_user['id'];//$this->session->userdata('ID');

        $array = array(
          'supplier_name' => $_POST[ 'company_name' ],
          'supplier_f_name' => $_POST[ 'contact_f_name' ],
          'supplier_l_name' => $_POST[ 'contact_l_name' ],
          'address' => $_POST[ 'address' ],
          'country' => $_POST[ 'country' ],
          'city' => $_POST[ 'city' ],
          'email' => $_POST[ 'email' ],
          'phone' => $_POST[ 'mobile_number' ],
          'website' => $_POST[ 'website_address' ],
          'twitter_id' => $_POST[ 'twitter_id' ],
          'user_id_fk' => $userId,
        );

        $this->suppliers->insertSupplier($array);

        $this->session->set_flashdata('success', '<span >Successfully Added Supplier</span>');
        redirect(base_url('supplier/add'));
      }
    }
    $data['all_suppliers'] = $this->suppliers->getAll($this->login_user['id']);

    $this->load->view('vw_add_supplier', $data);
  }

  /**
   *  Edit supplier
   */
  public function edit($supplier_id = NULL) {

  }

  public function view($supplier_id = NULL) {
    if (is_null($supplier_id)) {
      redirect(base_url('supplier'));
    }

    if (isset($_POST[ 'edit_user' ])) {
      foreach ($_POST as $field => $i) {
        $this->formValidationRules($field);
      }
      if ($this->form_validation->run() == FALSE) {

        $data['error'] = validation_errors();
        //$this->session->set_flashdata('error', '<span ></span>');
        //redirect(base_url('supplier/add'));
      }
      else {


        $userId = $this->login_user['id'];//$this->session->userdata('ID');

        $array = array(
            'supplier_name' => $_POST[ 'supplier_name' ],
            'supplier_f_name' => $_POST[ 'supplier_f_name' ],
            'supplier_l_name' => $_POST[ 'supplier_l_name' ],
            'address' => $_POST[ 'address' ],
            'country' => $_POST[ 'country' ],
            'city' => $_POST[ 'city' ],
            'email' => $_POST[ 'email' ],
            'phone' => $_POST[ 'phone' ],
            'website' => $_POST[ 'website' ],
            'twitter_id' => $_POST[ 'twitter_id' ],
            'user_id_fk' => $userId,
        );

        $this->suppliers->updateSupplier($array, $supplier_id);

        $this->session->set_flashdata('success', '<span >Successfully Edited Supplier</span>');
        redirect(base_url('supplier/view/'.$supplier_id));
      }
    }
    //taking data
    //@todo make it dynamic just taking supplier id for now possibly can take second param

    $supplier_t = $this->suppliers->getTransactions($this->login_user['id'], $supplier_id);
    $supplier = $this->suppliers->getOne($this->login_user['id'], $supplier_id);
    $all_supplier = $this->suppliers->getAll($this->login_user['id']);

    $up_meeting = $this->meetingmodel->getUpcoming($this->login_user['id'], $supplier_id);
    $this->load->view('vw_individual_company', compact('supplier_t', 'supplier', 'up_meeting', 'all_supplier'));
  }

  /**
   *  Export
   */
  public function export($supplier_id = NULL) {

  }


  /**
   * set form validation rules
   * @param $field
   *
   */
  protected function formValidationRules($field) {
    $rules = array(
      'company_name' => array(
        'Company Name',
        'required|max_length[50]'
      ),
      'contact_f_name' => array(
        'First Name',
        'required|max_length[50]'
      ),
      'contact_l_name' => array('Last Name', 'max_length[50]'),
      'address' => array('Address', 'max_length[255]'),
      'country' => array('Country', 'required'),
      'city' => array('City', 'required|max_length[50]'),
      'email' => array('Email', 'email'),
      'mobile_number' => array('Mobile Number', 'exact_length[10]|integer'),
      'website_address' => array(
        'Website Address',
        'max_length[255]|min_length[5]'
      ),
      'twitter_id' => array('Twitter Handler', 'max_length[50]|min_length[5]'),
    );

    if (isset($rules[ $field ])) {
      $this->form_validation->set_rules($field, $rules[ $field ][ 0 ],
        $rules[ $field ][ 1 ]);
    }

    //return TRUE;
  }

}