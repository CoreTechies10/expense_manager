<?php ;

/**
 * Created by PhpStorm.
 * User: Hitesh balyan(coolbalyan)
 * Date: 03/03/16
 * Time: 16:31
 */

/**
 * Class Users
 * @property CI_DB_active_record db
 */
class Users extends CI_Model {

  protected static $table = 'users';
  protected static $sub_table = '';

  public function __construct() {
    parent::__construct();
    //setting sub table
    $this->load->model('users_type');
    self::$sub_table = $this->users_type->getTable();
  }

  public function getTable() {
    return self::$table;
  }

  /**
   * @param string $user
   * @param string $pswd
   * @return stdClass|boolean
   */
  public function identifyUser($user = '', $pswd = '') {
    $where = '(u.company_name = \'' . $user . '\' OR ' . 'u.email = \'' . $user . '\')' . ' AND u.pswd = MD5(\'' . $pswd . '\')';
    $this->db->join(self::$sub_table . ' ut', 'u.user_type_fk=ut.ID');
    $this->db->select('u.*, ut.type, ut.ID as user_type_id');
    $result = $this->db->get_where(self::$table . ' u', $where, 1);

    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();

    if (!empty($result)) {
      return $result[ 0 ];
    }
    return FALSE;
  }

  /**
   * @param $set
   * @param string $idOrWhere
   * @return CI_DB_mysql_result|boolean
   */
  public function updateUser($set, $idOrWhere = '') {
    $where = is_numeric($idOrWhere) ? ' ID = ' . $idOrWhere : $idOrWhere;
    return $this->db->update(self::$table, $set, $where);
  }


}