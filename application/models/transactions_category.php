<?php /**
 * Created by PhpStorm.
 * User: Hitesh balyan(coolbalyan)
 * Date: 16/03/16
 * Time: 16:11
 */

/**
 * Class TransactionsCategory
 * @property CI_DB_active_record db
 */
class Transactions_Category extends CI_Model{

  protected static $table = 'transactions_category';

  public function getTable() {
    return self::$table;
  }


}