<?php

/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 17/03/16
 * Time: 15:59
 */
/**
 * Class Categories
 * @property CI_DB_active_record db
 */
class Categories  extends CI_Model {

  protected static $table = 'categories';
  protected static $sub_tables = [];


  public function __construct() {
    parent::__construct();
    //setting sub table
    $this->load->model('categories_data');
    self::$sub_tables['categories_data'] = $this->categories_data->getTable();
  }
  public function getTable() {
    return self::$table;
  }

  public function getSubTables($table = '') {
    return empty($table) ? self::$sub_tables : self::$sub_tables[ $table ];
  }

  public function getCategories(){

  }
}