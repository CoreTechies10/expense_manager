<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//namespace Login; wegwegwergwergwgweg wegwerg

/**
 * Class Registration
 */
class Home extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    /**
     *
     */
    public function index(){
        $this->load->view('vw_home');
    }

    public function registration(){
        if(isset($_POST['registration'])){
            $this->form_validation->set_rules('username', 'User Name', 'required|max_length[50]|is_unique[users.company_name]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[50]');
            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|is_unique[users.email]');

            if($this->form_validation->run() == false){
                //$this->session->set_flashdata('registration_fail', true);
                $this->load->view('vw_home', array('registration_fail' => true));
            }else{
                $array = array(
                    'company_name' => $this->input->post('username'),
                    'email' => $this->input->post('email'),
                    'user_type_fk' => 1,
                    'pswd' => md5($this->input->post('password')),
                    'confirmation_link' => md5($this->input->post('username').$this->input->post('email')),
                );

                $result = $this->db->insert('users', $array);
                if($result){
                    $this->load->helper('mail_helper');

                    $subject = 'Expense Manager Registration Mail';

                    $link = base_url().'registration/confirmation/'.$array['confirmation_link'];
                    // message for confirmation mail

                    // registration email template
                    $message = 'This is testing confirmation mail.<br>';

                    $message .= 'Please <a href="'.$link.'">click Here for confirmation</a>.<br>';
                    $message .= 'or simply go to the link: '. $link;

                    // call mail function for confirmation link
                    sendmail($array['email'], $message, $subject);
                    $this->session->set_flashdata('success', 'Registration created successfully and confirmation mail is send to user email for confirmation.');
                    redirect('home');

                }
            }
        }else{
            redirect('home');
        }
    }
}
