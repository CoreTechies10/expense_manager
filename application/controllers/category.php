<?php if (!defined('BASEPATH')) {
  exit('No direct script access allowed');
}


//namespace Login;

/**
 * Class Category
 */
class Category extends MY_Controller {

  public function __construct() {
    parent::__construct();

    if(!$this->login_user['id'] && !check_login_status($this->login_user['id'])){
        redirect('home');
    }
  }

  /**
   *
   */
  public function index() {

    /*
     * Add Meeting category
     */
    if (isset($_POST[ 'add_meeting_category' ])) {
      $this->form_validation->set_rules('meeting_type_category', 'Meeting Type',
        'required|max_length[50]');

      if ($this->form_validation->run() == TRUE) {
        $array = array(
          'category_type_id_fk' => 1,
          'value' => $this->input->post('meeting_type_category'),
          'user_id_fk' => $this->login_user['id'],//@todo change it
        );


        $result = $this->db->insert('categories_data', $array);

        if ($result) {
          $this->session->set_flashdata('success',
            'Meeting type added successfully');
          redirect('category');
        }
      }
    }

    /*
     * Add Meeting location
     */
    if (isset($_POST[ 'add_meeting_location' ])) {
      $this->form_validation->set_rules('meeting_type_location',
        'Meeting Location', 'required|max_length[50]');

      if ($this->form_validation->run() == TRUE) {
        $array = array(
          'category_type_id_fk' => 2,
          'value' => $this->input->post('meeting_type_location'),
          'user_id_fk' => $this->login_user['id'],//@todo change it
        );

        $result = $this->db->insert('categories_data', $array);
        if ($result) {
          $this->session->set_flashdata('success',
            'Meeting location added successfully');
          redirect('category');
        }
      }
    }

    /*
     * Add Meeting category
     */
    if (isset($_POST[ 'add_transaction_category' ])) {
      $this->form_validation->set_rules('transaction_category', 'Category',
        'required|max_length[50]');

      if ($this->form_validation->run() == TRUE) {
        $array = array(
          'category_type_id_fk' => 3,
          'value' => $this->input->post('transaction_category'),
          'user_id_fk' => $this->login_user['id'],//@todo change it
        );

        $result = $this->db->insert('categories_data', $array);

        if ($result) {
          $this->session->set_flashdata('success',
            'Category added successfully');
          redirect('category');
        }
      }
    }

    /*
     * Add Meeting category
     */
    if (isset($_POST[ 'add_transaction_payment' ])) {
      $this->form_validation->set_rules('transaction_payment', 'Payment','required|max_length[50]');

      if ($this->form_validation->run() == TRUE) {
        $array = array(
          'category_type_id_fk' => 4,
          'value' => $this->input->post('transaction_payment'),
          'user_id_fk' => $this->login_user['id'] ,//@todo change it
        );

        $result = $this->db->insert('categories_data', $array);

        if ($result) {
          $this->session->set_flashdata('success',
            'Payment added successfully');
          redirect('category');
        }
      }
    }

    if (isset($_POST[ 'edit_category' ])) {
      $this->form_validation->set_rules('category_id', 'ID', 'required');
      $this->form_validation->set_rules('value', 'Category',
        'required|max_length[50]');

      if ($this->form_validation->run() == TRUE) {
        $array = array(
          'value' => $this->input->post('value'),
        );
        $id = $this->input->post('category_id');
        $this->db->where('ID', $id);
        $result = $this->db->update('categories_data', $array);
        if ($result) {
          $this->session->set_flashdata('success',
            'Category edited successfully');
          redirect('category');
        }
      }
    }

    if (isset($_POST[ 'delete_category' ])) {
      $id = $this->input->post('category_id');
      $this->db->where('ID', $id);
      $result = $this->db->delete('categories_data');
      if ($result) {
        $this->session->set_flashdata('success',
          'Category deleted successfully');
        redirect('category');
      }
    }



    if (isset($_POST[ 'cancel_category' ])) {
      unset($_POST);
    }

    $this->load->view('vw_new_add_edit');
  }
}
