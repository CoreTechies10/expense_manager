<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Company Profile</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>--> 
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="col-lg-6 ">
          <div class=" exp-company-profile" >
            <h3 class="txt-blue"> Profile</h3>
            <table class="table table-bordered">
              <tr>
                <td>Comany Name</td>
                <td>XYZ PVT.ltd</td>
              </tr>
              <tr>
                <td>Contact First Name</td>
                <td>Jo<hn/td>
              </tr>
              <tr>
                <td>Contact Last Name</td>
                <td>Doe</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>Unit 1, Lloyds Wharf 2-3 Mill Street,</td>
              </tr>
              <tr>
                <td>City</td>
                <td>XYZ</td>
              </tr>
              <tr>
                <td>Country</td>
                <td>USA</td>
              </tr>
              <tr>
                <td>Tel No.</td>
                <td>+44 (0)207 367 6331</td>
              </tr>
              <tr>
                <td>Website Address</td>
                <td><a href="#">www.johndoe.com</a></td>
              </tr>
              <tr>
                <td>Twitter ID</td>
                <td><a href="#">hello@demos</a></td>
              </tr>
              <tr>
                <td>Account Type</td>
                <td>Standard</td>
              </tr>
            </table>
            
            	 <div class="text-right">
                <button class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edit</button>
              </div>
            
          </div>
          
        </div>
       
             
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="min-height:66vh;">
            <div >
              <div class="col-lg-6 null-padding" >
                <h3 class="txt-cyan"> Company Balance</h3>
              </div>
              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"> <span class="label label-danger txt-big">-$3700</span> </div>
              <div class="clearfix"></div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
                <tr class=" bg-blue bg-gray">
                  <th></th>
                  <th>Total Paid</th>
                  <th>Total Delivery Value</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th >Food Supply</th>
                  <td>$2000</td>
                  <td>$500</td>
                  <td><span class="label label-success">$1500</span></td>
                </tr>
                <tr>
                  <th>Uniform Supply</th>
                  <td>$5000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$2000</span></td>
                </tr>
                <tr>
                  <th>Snacks</th>
                  <td>$1000</td>
                  <td>$200</td>
                  <td><span class="label label-success ">$800</span></td>
                </tr>
                <tr>
                  <th>Accessories</th>
                  <td>$2000</td>
                  <td>$1000</td>
                  <td><span class="label label-success ">$1000</span></td>
                </tr>
                <tr>
                  <th>Water</th>
                  <td>$3000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$4000</span></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"><span class="txt-big">Total Balance</span></td>
                  <td></td>
                  <td><span class="txt-big"><b>-$3700</b></span></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"  style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav> <span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span> </nav>
              <h2 id="custom-month" class="custom-month"></h2>
              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>