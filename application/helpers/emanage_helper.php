<?php

if(!function_exists('check_login_status')){
    function check_login_status($user_id = null, $check_session = false){
        // check user login status
        $ci = & get_instance();
        if(isset($user_id) && $user_id != null){
           $ci->db->where('ID', $user_id);
            if($ci->db->count_all_results('users')>0){
                if($check_session){
                    if($ci->login_user && $user_id == $ci->login_user['id']){
                        return true;
                    }else{
                        return false;
                    }
                }else{
                    return true;
                }
            }else{
                return false;
            }

        }else{
            if($check_session && $ci->login_user){
                return true;
            }else{
                return false;
            }
        }
    }
}

if(!function_exists('get_user_record')){
    function get_user_record($user_id = null){
        if($user_id && $user_id != null){
            $ci = & get_instance();
            $ci->db->where('u.ID', $user_id);
            $ci->db->join('users_type ut', 'u.user_type_fk=ut.ID');
            $ci->db->select('u.*, ut.type');
            $result = $ci->db->get('users u');
            if($result->num_rows>0){
                $result = $result->result_array();
                return $result[0];
            }else{
                return false;
            }
        }
    }
}

if(!function_exists('get_category_list')){
    function get_category_list($category_id = null, $category_type_id = null, $user_id = null){
        $ci = & get_instance();

        //
        if($category_id != null){
            $ci->db->where('ID', $category_id);
        }

        //
        if($category_type_id != null){
            $ci->db->where('category_type_id_fk', $category_type_id);
        }

        //
        if($user_id != null){
            $ci->db->where('user_id_fk', $user_id);
        }

        return $ci->db->get('categories_data');



    }
}
  