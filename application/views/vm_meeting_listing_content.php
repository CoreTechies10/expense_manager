



<!-- Table Calander-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="exp-tables padding-top0">
            <header>




            </header>
            <table class="table table-bordered dataTable">
              <thead>
              <tr class=" bg-blue bg-gray">
                <th>Meeting</th>
                <th>Date</th>
                <th></th>


              </tr>
              </thead>

              <tbody>
              <?php
              $i=0;
              if($meeting){

                foreach($meeting as $row)
              {

                ?>
              <tr>


                <td><?php echo $row['name'];?></td>
                <td><?php echo $row['datetime'];?></td>
                <td><a href=" <?php echo base_url('meeting/detail/'.$row['ID'])  ?>" class="label label-info">Detail</a></td>


              </tr>
              <?php  }}?>

              </tbody>
            </table>
          </div>
        </div>

        <div class="clearfix"></div>

      </div>


    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"  style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav> <span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span> </nav>
              <h2 id="custom-month" class="custom-month"></h2>
              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>