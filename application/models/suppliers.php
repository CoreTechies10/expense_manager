<?php

/**
 * Created by PhpStorm.
 * User: Hitesh Balyan
 * Date: 03-03-2016
 * Time: 09:42 PM
 */

/**
 * Class Users
 * @property CI_DB_active_record db
 */
class Suppliers extends CI_Model {

  protected static $table = 'suppliers_profile';
  protected static $sub_tables = array();


  public function __construct() {
    parent::__construct();
    //setting sub table
    $this->load->model('transactionsmodel');
    $this->load->model('meetingmodel');
    self::$sub_tables[ 'transactionsmodel' ] = $this->transactionsmodel->getTable();
    self::$sub_tables[ 'meetingmodel' ] = $this->meetingmodel->getTable();
  }

  public function getTable() {
    return self::$table;
  }

  public function getSubTables($table = '') {
    return empty($table) ? self::$sub_tables : self::$sub_tables[ $table ];
  }

  public function getAll($userId = '') {
    if (empty($userId)) {
      $userId = $this->login_user['id'];
      if ($userId === FALSE) {
        return array();
      }
    }

    $result = $this->db->get_where(self::$table, 'user_id_fk = ' . $userId);
    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();
    if (!empty($result)) {
      return $result;
    }
    return array();
  }

  public function getOne($userId = '', $supplier = '') {
    if (empty($supplier)) {
      return array();
    }
//    if (empty($userId)) {
//      $this->load->library('session');
//      $userId = $this->session->userdata('ID');
//      if ($userId === FALSE) {
//        return FALSE;
//      }
//    }

    $where = array('ID' => $supplier);
    if (!empty($userId)) {
      $where[ 'user_id_fk ' ] = $userId;
    }

    $result = $this->db->get_where(self::$table, $where, 1);
    //SELECT * FROM suppliers WHERE user_id_fk = 1
    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();

    if (!empty($result)) {
      return $result[ 0 ];
    }
    return FALSE;
  }

  public function getTransactions($userId = '', $supplerId = '', $meetingId = '') {
    if (empty($userId)) {
      $userId = $this->login_user['id'];
      if ($userId === FALSE) {
        return array();
      }
    }

    $where = array('s.user_id_fk' => $userId);
    if (!empty($supplerId)) {
      $where[ 't.supplier_id_fk' ] = $supplerId;
    }

    if (!empty($meetingId)) {
      $where[ 't.meeting_id_fk' ] = $meetingId;
    }

    $this->db->join(self::$table . ' s', 's.ID=t.supplier_id_fk');
    $this->db->join($this->getSubTables('meetingmodel') . ' m', 'm.ID=t.meeting_id_fk');
    $result = $this->db->get_where($this->getSubTables('transactionsmodel') . ' t', $where);


    //@var CI_DB_mysql_result $result;
    $result = $result->result_array();
    if (empty ($result)) {
      return $result;
    }

    foreach ($result as $key => $val) {
      $this->load->model('categories_data');
      $this->load->model('meetingmodel');
      $tmp_meeting_result = $this->categories_data->getMeetingCategory($userId, $val[ 'meeting_id_fk' ]);

      $tmp_transaction_result = $this->categories_data->getTransactionCategory($userId, $val[ 'meeting_id_fk' ],
        $val[ 'supplier_id_fk' ]);

      $result[ $key ] += array(
        'meeting_categories' => $tmp_meeting_result,
        'transaction_categories' =>
          $tmp_transaction_result
      );
    }

    return $result;
  }

  /**
   * @param $set
   * @param string $idOrWhere
   * @return CI_DB_mysql_result|boolean
   */
  public function updateSupplier($set, $idOrWhere = '') {
    //$where = !empty($idOrWhere) ? ' ID = ' . $idOrWhere : $idOrWhere;
    if($idOrWhere != ''){
      $this->db->where('ID', $idOrWhere);
    }
    return $this->db->update(self::$table, $set);
  }

  public function remove($id) {
    if ($id === NULL) {
      return FALSE;
    }
    return $this->db->delete(self::$table, ['ID' => $id]);
  }

  public function insertSupplier($data = []) {
    if (empty($data)) {
      return FALSE;
    }
    return $this->db->insert(self::$table, $data);
  }

}