<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $login_user;
    public function __construct()
    {
        parent::__construct();

        // load library form validation
        $this->load->library('form_validation');
        $this->load->helper('emanage_helper');


        if($this->session->userdata('login_user')){
            $this->login_user = $this->session->userdata('login_user');
        }

    }
}