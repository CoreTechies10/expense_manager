<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//namespace Login;

/**
 * Class Registration
 */
class Registration extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    /**
     *
     */
    public function index(){
        if(isset($_POST['registration'])){
            $this->form_validation->set_rules('username', 'User Name', 'required|max_length[50]|is_unique[users.company_name]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[50]');
            $this->form_validation->set_rules('email', 'Email', 'required|max_length[255]|is_unique[users.email]');

            if($this->form_validation->run() == false){
                $this->session->set_flashdata('registration_fail', true);
                redirect('home');
            }else{

            }
        }else{
            show_404();
        }
    }

    public function confirmation($link = null){
        if(isset($link) && $link != null){
            $this->db->where('confirmation_link', $link);
            $result = $this->db->get('users');
            if($result->num_rows>0){
                //echo 'bhosdike mil gya, ab aage';
                $result = $result->result_array();
                if($result[0]['confirmation_status'] == 0){
                    $data = array('message' => 'Thanks for confirmation, please login and continue registration process');
                    $this->db->update('users', array('confirmation_status' => 1));

                }else{
                    $data = array('message' => 'Sorry, You are already confirm with this account. Please login with your account');
                }
                $this->load->view('vw_confirmation', $data);
            }else{
                //echo 'bhaiya ussko kuch mila nahi'; 9314031585
            }
        }else{
            show_404();
        }
    }
}
