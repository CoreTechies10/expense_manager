<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em; font-family: 'OpenSans-Semibold'"><i class="fa fa-truck"></i>  AbcFood Williams (Will)</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Supplier Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" id='company-profile'>
            <h3 class="txt-blue"> Profile</h3>
            <table class="table table-bordered">
              <tr>
                <td>Company Name</td>
                <td>ABC Food</td>
              </tr>
              <tr>
                <td>Contact First Name</td>
                <td>Williams</td>
              </tr>
              <tr>
                <td>Contact Last Name</td>
                <td>Will</td>
              </tr>
              <tr>
                <td>Address</td>
                <td>Unit 1, Lloyds Wharf 2-3 Mill Street,</td>
              </tr>
              <tr>
                <td>City</td>
                <td>XYZ</td>
              </tr>
              <tr>
                <td>Country</td>
                <td>USA</td>
              </tr>
              <tr>
                <td>Tel No.</td>
                <td>+44 (0)207 367 6331</td>
              </tr>
              <tr>
                <td>Website Address</td>
                <td><a href="#">www.example.com</a></td>
              </tr>
              <tr>
                <td>Twitter ID</td>
                <td><a href="#">hello@demos</a></td>
              </tr>
             
            </table>
            
             <div class="text-right">
                <button class="btn btn-xs btn-info"><i class="fa fa-edit"></i> Edit</button>
              </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile" id='company-expense'>
            <div >
              <div class="col-lg-6 null-padding">
                <h3 class="txt-cyan"> Company Balance</h3>
              </div>
              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"> <span class="label label-danger txt-big">-$3700</span> </div>
              <div class="clearfix"></div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
                <tr class="bg-blue bg-gray">
                  <th></th>
                  <th>Total Paid</th>
                  <th>Total Delivery Value</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th>Food Supply</th>
                  <td>$2000</td>
                  <td>$500</td>
                  <td><span class="label label-success">$1500</span></td>
                </tr>
                <tr>
                  <th>Uniform Supply</th>
                  <td>$5000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$2000</span></td>
                </tr>
                <tr>
                  <th>Snacks</th>
                  <td>$1000</td>
                  <td>$200</td>
                  <td><span class="label label-success ">$800</span></td>
                </tr>
                <tr>
                  <th>Accessories</th>
                  <td>$2000</td>
                  <td>$1000</td>
                  <td><span class="label label-success ">$1000</span></td>
                </tr>
                <tr>
                  <th>Water</th>
                  <td>$3000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$4000</span></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"><span class="txt-big">Total Balance</span></td>
                  <td></td>
                  <td><span class="txt-big"><b>-$3700</b></span></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table Calander-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span class="exp-title-1 txt-blue"> Delivery Value from AbcFood Williams (Will) </span></h3>
          </div>
          <div class="col-lg-5 text-right exp-delivery"> 
          		 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
                 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
                 <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
                 <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>
                 
          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered exp-data-table" id="delivery-table">
          <thead>
            <tr class="bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Pay Type</th>
              <th>Desc</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>AbcFood</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$1000</td>
              
            </tr>
             <tr>
              <td>01-Sep-2015</td>
              <td>AbcFood</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$1000</td>
              
            </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
          <header>
        <div class="col-lg-7">
          <h3><i class="fa fa-money"></i> <span class="exp-title-1 txt-blue">Amount Paid in Advance to AbcFood Williams (Will)</span></h3>
        </div>
         <div class="col-lg-5 text-right exp-amount"> 
          		 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
                 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
                 <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
                 <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>
          </div>
          
        <div class="clearfix"></div>
        </header>
        <table class="table dataTable table-bordered">
          <thead>
            <tr class="bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Pay Type</th>
              <th>Desc</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>AbcFood</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$300</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
      <header>
        <div class="col-lg-7">
          <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">UpComming Meetings</span></h3>
        </div>
         <div class="col-lg-5 text-right exp-upcomming"> 
          		 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Row</button>
                 <button type="button" class="btn btn-xs btn-success exp-addRow-btn"><i class="fa fa-plus"></i> Add Column</button>
                 <button type="button" class="btn btn-xs btn-info exp-edit-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-primary exp-editinline-btn"><i class="fa fa-edit"></i> Edit</button>
                 <button type="button" class="btn btn-xs btn-danger  exp-delete-btn"><i class="fa fa-trash-o"></i> Delete</button>
                 <button type="button" class="btn btn-xs btn-default exp-cancel-btn"><i class="fa fa-trash-o"></i> Cancel</button>
          </div>
        <div class="clearfix"></div>
      </header>
        <table class="table dataTable table-bordered">
          <thead>
            <tr class="bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Location</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>AbcFood</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>HIltonCD</td>
              <td>$300</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"  style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 event-section">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav> <span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span> </nav>
              <h2 id="custom-month" class="custom-month"></h2>
              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>