<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add Meeting</h3>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent; min-height:465px;">
    <div class="row">
        <div class="container">
            <form action="<?php echo base_url();?>meeting/add" method="post" id="-form-meeting-create" accept-charset="UTF-8">
                <div>
                    <div class="row"><div class="col-md-12">
                            <input class="pull-right btn btn-xs btn-success form-submit" type="submit" id="edit-submit" name="meeting" value="Save">
                        </div>
                    </div>
                    <div id="edit-table-meeting" class="form-wrapper"></div>
                    <table class="table table-bordered">
                        <thead>
                        <tr class=" bg-blue bg-gray">
                            <th>Name</th>
                            <th>Type</th>
                            <th>Location</th>
                            <th>Date</th>
                        </tr>
                        <tr><td>
                                <div class="form-item form-type-textfield form-item-meeting-name">
                                    <input class="form-control form-text" placeholder="Meeting Name" type="text" id="edit-meeting-name" name="meeting_name" value="" size="25" maxlength="128">
                                </div>
                            </td><td>
                                <div class="form-item form-type-select form-item-meeting-type">

                                    <select class="form-control form-select" id="edit-meeting-type" name="meeting_type">
                                        <?php
                                        $result=get_category_list('','1',$this->login_user['id']);
                                        $result=$result->result();
                                        if($result){

                                            foreach($result as $row)
                                            {
                                                ?>

                                                <option value="<?php echo $row->ID;?>">  <?php echo $row->value;?></option>
                                                <?php
                                            } }?>

                                    </select>
                                </div>
                            </td><td><div class="form-item form-type-select form-item-meeting-location">
                                    <select class="form-control form-select" id="edit-meeting-location" name="meeting_location">
                                        <?php
                                        $result=get_category_list('','2',$this->login_user['id']);
                                        $result=$result->result();
                                        if($result){
                                            foreach($result as $row)
                                            {
                                                ?>

                                                <option value="<?php echo $row->ID;?>">  <?php echo $row->value;?></option>
                                            <?php } }?>
                                    </select>
                                </div>
                            </td><td>
                                <div class="form-item form-type-textfield form-item-meeting-date">
                                    <input class="datetime form-control form-text" placeholder="1992-05-04" type="date" id="edit-meeting-date" name="meeting_date" value="" size="15" maxlength="128">
                                </div>
                            </td></tr></thead></table>
                    <div id="edit-table-supplier" class="form-wrapper"></div>
                    <table class="table table-bordered">
                        <thead>
                        <tr class=" bg-blue bg-gray">
                            <th>Company</th>
                            <th>First Name</th>
                            <th>Category</th>
                            <th>Payment</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Delivered</th>
                            <th>Advance</th>
                            <th></th>
                        </tr></thead><tbody>
                        <?php
                        $i=0;
                        if($transction_list){
                            foreach($transction_list as $row)
                            {
                                $i++;
                                //var_dump($row);
                                ?>
                                <tr>
                                    <td ><?php echo $row->company_name;?></td>
                                    <td ><?php echo $row->supplier_name;?></td>
                                    <td>

                                        <div class="form-item form-type-select form-item-category-16">
                                            <select class="form-control form-select" id="category<?php echo $i;?>"  name="supplier[<?= $i ?>][category][category]">
                                                <?php
                                                $result=get_category_list('','3',$this->login_user['id']);
                                                $result=$result->result();
                                                if($result){
                                                    foreach($result as $category)
                                                    {
                                                        ?>

                                                        <option    value="<?php echo $category->ID;?>"> <?php echo $category->value;?></option>
                                                    <?php } }?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-item form-type-select form-item-payment-16">
                                            <select class="form-control form-select" id="edit-payment-16" name="supplier[<?= $i ?>][category][payment]">
                                                <?php
                                                $result=get_category_list('','4',$this->login_user['id']);
                                                $result=$result->result();
                                                if($result){
                                                    foreach($result as $payment)
                                                    {
                                                        ?>

                                                        <option data-payment="<?php echo $payment->value;?>" value="<?php echo $payment->ID;?>">  <?php echo $payment->value;?></option>
                                                    <?php } }?>
                                            </select>
                                        </div>
                                    </td>
                                    <td>

                                        <div class="form-item form-type-textfield form-item-amount-16">
                                            <input class="form-control form-text" data-amount="<?php echo $i;?>" type="text" id="edit-amount-16" name="supplier[<?= $i ?>][amount]" value="" size="10" maxlength="128">
                                        </div>
                                    </td>
                                    <td>
                                        <div class="form-item form-type-textarea form-item-desc-16">
                                            <div class="form-textarea-wrapper resizable textarea-processed resizable-textarea">
                                                <textarea class="form-control form-textarea" id="edit-desc-16" name="supplier[<?= $i ?>][desc]" cols="10" rows="1"></textarea>
                                                <div class="grippie"></div></div>
                                        </div>
                                    </td>
                                    <td><div class="form-item form-type-select form-item-delivery-16">
                                            <select class="form-control form-select" id="edit-delivery-16" name="supplier[<?= $i ?>][delivery]"><option value="no">No</option><option value="yes">Yes</option></select>
                                        </div>
                                    </td><td><div class="form-item form-type-select form-item-advance-16">
                                            <select class="form-control form-select" id="edit-advance-16" name="supplier[<?= $i ?>][advance]"><option value="no">No</option><option value="yes">Yes</option></select>
                                        </div>
                                    </td><input type="hidden" name="supplier[<?= $i ?>][id]" value="<?= $row->ID ?>"/> <td>
                                        <input class="btn btn-sm btn-info form-submit ajax-processed" data-suppliername="<?php echo $row->supplier_name;?>"    data-companyname="<?php echo $row->company_name;?> "  data-val="<?php echo $i;?>"                      type="button" id="edit-action-16" name="<?php echo $i;?>" value="+Add"/></td></tr>
                            <?php } }?>
                        </tbody></table>
                    <input type="hidden" name="form_build_id" value="form-LBhaUTKZNdqBfZ8jyczXHS2UozAwhZqEexm2nwGdsGE">
                    <input type="hidden" name="form_token" value="uT2CBE7o8PFt10o_nsfKp1_nYEkUqJRwkgiPcMTlZko">
                    <input type="hidden" name="form_id" value="_form_meeting_create">
                </div>
            </form>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <thead>
                        <tr class=" bg-blue bg-gray">
                            <th>Company</th>
                            <th>First Name</th>
                            <th>Category</th>
                            <th>Payment</th>
                            <th>Amount</th>
                            <th>Description</th>
                            <th>Delivered</th>
                            <th>Advance</th>
                        </tr>
                        </thead>
                        <tbody id="supplier_lists">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){

        $('.ajax-processed').click(function(){
            var val = $(this).attr('data-val');
            //alert(val);
            if(val !=''){


                var compny = $(this).attr('data-companyname');
                var supplier = $(this).attr('data-suppliername');
                var category_id = $('select[name="supplier['+val+'][category][category]"]').val();
                //alert(category_id);

                var payment_id = $('select[name="supplier['+val+'][category][payment]"]').val();
                var amount_id = $('input[name="supplier['+val+'][amount]"]').val();
                var desc = $('textarea[name="supplier['+val+'][desc]"]').val();

                var delivery = $('select[name="supplier['+val+'][delivery]"]').val();

                var advance = $('select[name="supplier['+val+'][advance]"]').val();
                $.ajax({
                    type: "POST",
                    url: baseurl+"transactions/send_value_ajax",
                    data: {category_id: category_id, payment_id:payment_id, delivery:delivery,advance:advance,compny:compny,supplier:supplier,amount_id:amount_id,desc:desc,delivery:delivery,advance:advance,value:val},
                    dataType: "json",
                    success:function(result){
                        //console.log(result)
                        if(result['status'] == true){

                            $('#supplier_lists').append(result["message"]);
                        }
                    }
                });






            }
            $(this).hide();

        } );
    });

</script>