<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Add Seller Company </title>
<link href="<?php echo ASSEST_PATH; ?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo ASSEST_PATH; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!--Link Effects css-->
<link href="<?php echo ASSEST_PATH; ?>css/component.css" rel="stylesheet" type="text/css">
<!--Calander css-->
<link rel="stylesheet" type="text/css" href="<?php echo ASSEST_PATH; ?>css/calendar.css" />
<link rel="stylesheet" type="text/css" href="<?php echo ASSEST_PATH; ?>css/custom_2.css" />
<!--Scroll css-->
<link rel="stylesheet" href="<?php echo ASSEST_PATH; ?>css/jquery.mCustomScrollbar.css">
<link rel="stylesheet" type="text/css" href="<?php echo ASSEST_PATH; ?>css/dataTables.bootstrap.min.css" />
<link href="<?php echo ASSEST_PATH; ?>css/style.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script type="application/javascript" > baseurl="<?php echo base_url()?>"</script>
</head>

<body >
<header class="container-fluid exp-header">
  <div class="row">
    <div class="container">
      <div class="col-lg-3"> <a href="<?php echo base_url().'home'; ?>"><span class="exp-logo">Expense<span class="txt-cyan"> Manager</span></span></a> </div>
      <div class="col-lg-9 exp-menu">
        <ul class="cl-effect-1">
          <?php
          if($this->login_user){
            //$user_data = $this->session->userdata('login_user');
          ?>
           <!-- <li class="dropdown"> <a href="#" id="company" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company</a>
              <ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="company">
                <li><a href="#">Company Profile</a></li>
                <li><a href="#">Add/Edit Company</a></li>
              </ul>
            </li>-->
              <li class="dropdown"> <a href="<?php echo base_url() ?>category" >Catagory</a>
                  <!--<ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="catagory">
                    <li><a href="#">Catagory Type</a></li>
                    <li><a href="#">Catagory List</a></li>
                    <li><a href="#">Add/Edit Catagory List</a></li>
                  </ul>-->
              </li>
            <li class="dropdown"> <a href="#" id="meetings" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Meetings</a>
              <ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="meetings">
                <li><a href="<?php echo base_url().'transactions' ?>">Meetings Create</a></li>
                <li><a href="<?php echo base_url().'meeting/listing' ?>">Meetings List</a></li>
              </ul>
            </li>
              <li class="dropdown"> <a href="<?php echo base_url() ?>supplier/add" >Suppliers</a>
                  <!--<ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="catagory">
                    <li><a href="#">Catagory Type</a></li>
                    <li><a href="#">Catagory List</a></li>
                    <li><a href="#">Add/Edit Catagory List</a></li>
                  </ul>-->
              </li>
            <!--  <li class="dropdown"> <a href="#" id="payments" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Payments</a>
              <ul class="dropdown-menu dropdown-menu-right exp-dropdown-menu" aria-labelledby="payments">
                 <li><a href="#">Payments Type</a></li>
                 <li><a href="#">Payments List</a></li>
                 <li><a href="#">Add/Edit Payments List</a></li>
               </ul>
             </li>-->
            <!--<li><a href="#" >Reports</a></li>
            <li class="dropdown"> <a href="#" id="search" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-search"></i></a>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="search" style="width:250px;">
                <form>
                  <li>
                    <div class="col-lg-12" style="padding:0px;">
                      <div class="input-group"> <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                      </span>
                        <input type="text" class="form-control" placeholder="Search for...">
                      </div>

                    </div>

                  </li>
                </form>
              </ul>
            </li>-->
            <li class="dropdown"> <a href="#" id="login" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url().'images/user.jpg' ?>" alt="user" class="user-img"> <?php echo $this->login_user['company_name'] ?></a>
              <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="login">
                <li class="text-center"><a href="<?php echo base_url().'user/profile/'.$this->login_user['id'] ?>" style="font-size:14px !important;">My Profile</a></li>
                <li class="divider"></li>
                <li class="text-right">
                  <button  onclick="window.location='<?php echo base_url() ?>login/logout';" class="btn btn-sm btn-info btn-block" style="font-size:14px !important;"> Logout</button>
                </li>
              </ul>
            </li>

          <?php } else { ?>
            <li><a href="#" id="login" type="button" data-toggle="modal" data-target="#signin"><i class="fa fa-user"></i> Login</a></li>
            <li><a href="#" type="button" type="button" data-toggle="modal" data-target="#signup" ><i class="fa fa-user"></i> Register</a></li>
          <?php } ?>

        </ul>
      </div>
    </div>
  </div>
</header>