<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Meeting Name </h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
              
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="col-lg-6" >
          <div class=" exp-company-profile" style="height:220px;">
            <h4 class="txt-cyan" style="margin-bottom:5px;">Detail </h4>
            <table class="table table-bordered">
              <tr>
                <td>Meeting Name</td>
                <td>xyz</td>
              </tr>
              <tr>
                <td>Meeting Type</td>
                <td>New Project</td>
              </tr>
              <tr>
                <td>Meeting Date</td>
                <td>26/09/2015</td>
              </tr>
              <tr>
                <td>Meeting Location</td>
                <td>HitonWC</td>
              </tr>
              
            </table>
            
            	 
            
          </div>
          
        </div>
       
             
        <div class="col-lg-6">
          <div class="exp-company-profile" style="height:220px;" >
            <div >
              <div class="col-lg-6 null-padding" >
                <h4 class="txt-cyan" style="margin-bottom:5px;"> Company Balance </h4>
              </div>
              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"> </div>
              <div class="clearfix"></div>
            </div>
            <div style="max-height:170px; overflow:hidden; overflow-y:auto;">
            	<table class="table table-responsive table-bordered exp-table">
              <thead>
                <tr class=" bg-blue bg-gray">
                  <th></th>
                  <th>Total Paid</th>
                  <th>Total Delivery Value</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                
                <tr>
                  <th>Snacks</th>
                  <td>$1000</td>
                  <td>$200</td>
                  <td><span class="label label-success ">$800</span></td>
                </tr>
                <tr>
                  <th>Accessories</th>
                  <td>$2000</td>
                  <td>$1000</td>
                  <td><span class="label label-success ">$1000</span></td>
                </tr>
                
               
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"><span class="txt-big">Total Balance</span></td>
                  <td></td>
                  <td><span class="txt-big"><b>-$3700</b></span></td>
                </tr>
              </tfoot>
            </table>
            </div>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table Calander-->
<div class="container-fluid exp-main-container" style="padding-top:20px; padding-bottom:20px;" >
  <div class="row">
    <div class="container">
      
      
      <div class="exp-tables">
        <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">Supplier Details</span></h3>
        <div class="col-lg-12 div-table">
            	<div class="div-row bg-blue bg-gray">
                	
                	<div class="div-cell">Company Name</div>
                    <div class="div-cell">Contact Name </div>
                   	<div class="div-cell"> Email</div>
                   	<div class="div-cell"> Tel. NO.</div>
                    <div class="div-cell"> Payment Type</div>
                    <div class="div-cell"> Amount</div>
                    <div class="div-cell"> Save/ Edit</div>
                </div>
                <div class="div-row bg-white">
                	
                	<div class="div-cell"><input class="border-0" type="text" value="AbcFood" readonly/></div>
                    <div class="div-cell"><input class="border-0" type="text" value="Willums" readonly/></div>
                    <div class="div-cell"><input class="border-0" type="text" value="willums@example.com" readonly/></div>
                    <div class="div-cell"><input class="border-0" type="text" value="+91 9999999999" readonly/></div>
                   	<div class="div-cell"><input class="border-0" type="txt" value="advance" readonly/></div>
                    <div class="div-cell"><input class="border-0" type="txt" value="$1000" readonly/></div>
                    <div class="div-cell"> 
                    	<button class="btn btn-xs btn-info" type="button"><i class="fa fa-edit"></i></button>
                        <button class="btn btn-xs btn-danger" type="button"><i class="fa fa-trash-o"></i></button>
                        
                    </div>
                </div>
            </div>
      </div>
    </div>
  </div>
</div>