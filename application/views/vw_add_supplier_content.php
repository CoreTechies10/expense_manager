<div class="container-fluid">
  <div class="row">
    <div class="container">
      <!-- show success message -->
      <?php if ($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable" style="margin-top: 20px">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif; ?>

      <!-- show error message -->
      <?php if (isset($error)): // $this->session->flashdata('error')?>
        <div class="alert alert-danger alert-dismissable" style="margin-top: 20px">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo  $error; //$this->session->flashdata('error'); ?>
        </div>
      <?php endif; ?>
      <!-- End show success message -->

      <div class="col-lg-12 exp-company-profile "
           style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add Supplier</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3 hidden">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style="line-height:2em;"> Company Names :</td>
                <td>
                  <button type="button"
                          class="btn btn-default dropdown-toggle btn-xs btn-block"
                          data-toggle="dropdown" aria-haspopup="true"
                          aria-expanded="false"><i class="fa fa-bars"></i> <span
                      class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#">AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul>
                </td>

              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">

  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile">
            <h3 class="txt-blue"> Profile</h3>

            <form class="form-horizontal" novalidate method="post" action="<?= base_url('supplier/add') ?>">
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Company Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="company_name" value="<?php echo set_value('company_name'); ?>" placeholder="enter company name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact F-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contact_f_name" value="<?php echo set_value('contact_f_name'); ?>" placeholder="enter first name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact L-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contact_l_name" value="<?php echo set_value('contact_l_name'); ?>" placeholder="enter last name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Address :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="address" value="<?php echo set_value('address'); ?>" placeholder="enter address" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Country :</label>
                </div>
                <div class="col-lg-3">
                  <input type="text" class="form-control " name="country" value="<?php echo set_value('country'); ?>" placeholder="country name" required>
                </div>
                <div class="col-lg-2">
                  <label class="control-label"> City :</label>
                </div>
                <div class="col-lg-3">
                  <input type="text" class="form-control " name="city" value="<?php echo set_value('city'); ?>" placeholder="city name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Email :</label>
                </div>
                <div class="col-lg-8">
                  <input type="email" class="form-control " name="email" value="<?php echo set_value('email'); ?>" placeholder="enter email ID" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Tel No. :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="mobile_number" value="<?php echo set_value('mobile_number'); ?>" placeholder="enter phone no" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Website Address :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="website_address" value="<?php echo set_value('website_address'); ?>" placeholder="enter website " required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Twitter ID :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="twitter_id" value="<?php echo set_value('twitter_id'); ?>" placeholder="enter twitter ID " required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-lg-12 text-right">
                <input type="submit" class="btn btn-xs btn-success" name="supplier_add" value="Add">

              </div>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"
               style="height:68.5vh ; padding:5px;;">
            <table class="table">
              <thead>
              <tr>
                <td>Suppliers</td>
                <td>Details</td>
              </tr>
              </thead>
              <tbody>
              <?php
              if (!$all_suppliers) {
                echo '<tr>
                        <td>No Supplier Found</td>
                        <td><a href="" title="">--</a></td>
                      </tr>';
              }
              else {
                foreach ($all_suppliers as $supplier) {
                  echo '<tr>
                          <td>' . $supplier['supplier_name'] . '</td>
                          <td><a href="' . base_url('supplier/view/' . $supplier['ID']) . '" title="">View</a></td>
                        </tr>';
                }
              }
              ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>