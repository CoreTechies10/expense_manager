<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: Hitesh Balyan
 * Date: 03-03-2016
 * Time: 09:52 PM
 */

/**
 * Class Transactions
 * @property CI_DB_active_record db
 */
class transactionsmodel extends CI_Model {

  protected static $table = 'transactions';
  protected static $sub_tables = [];


  public function __construct() {
    parent::__construct();
    //setting sub table
    $this->load->model('transactions_category');
    $this->load->model('transactions_attch');
    self::$sub_tables['transactions_category'] = $this->transactions_category->getTable();
    self::$sub_tables['transactions_attch'] = $this->transactions_attch->getTable();

  }

  public function getTable() {
    return self::$table;
  }

  public function getSubTables($table = '') {
    return empty($table) ? self::$sub_tables : self::$sub_tables[ $table ];
  }

    public function addTransaction($data, $categories, $attachment = [])
    {

        foreach ($data as $transaction) {
            $transactionId = $this->db->insert(self::$table, $transaction);
            if ($transactionId == FALSE) {
                return FALSE;
            }
            $transactionId = $this->db->insert_id();
            $attachmentIds = array();
            $tmp_category = array();

            foreach ($categories as $key =>$category) {
                $tmp_category[] = array('category_data_id_fk' => $category['category_data_id_fk'], 'transaction_id_fk' => $transactionId);
            }
            $categories = $tmp_category;

            $categoryIds = $this->db->insert_batch($this->getSubTables('transactions_category'), $categories);

            if (!empty($attachment)) {
                $attachment['transaction_id_fk'] = $transactionId;
                $attachmentIds = $this->db->insert_batch($this->getSubTables('transactions_attch'), $attachment);
            }
//          return array(
//              'transactionId' => $transactionId,
//              'categoryId' => $categoryIds,
//              'attachmentIds' => $attachmentIds
//          );
        }
    }

  public function removeTransaction($id = '') {
    if (empty($id)) {
      return FALSE;
    }
    return $this->db->delete(self::$table, 'ID=' . $id);
  }

  function TranscationListing($limit, $start)
  {
    $this->db->select('users.company_name,suppliers_profile.supplier_name,suppliers_profile.ID');
    $this->db->from('users');

    $this->db->join('suppliers_profile', 'users.ID = suppliers_profile.user_id_fk');

    $this->db->where('users.ID', $this->login_user['id']);

    $this->db->order_by('users.company_name','DESC');
    $this->db->limit($limit,$start);
    $query = $this->db->get();

   // echo $this->db->last_query();die();
    if ($query->num_rows() > 0) {
      foreach ($query->result() as $row) {
        $data[] = $row;
      }
      return $data;
    }
    else
      return false;
  }

}
