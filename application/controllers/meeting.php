<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Meeting extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        //check for user login
        $this->load->model('MeetingModel');
        if (!$this->login_user['id'] && !check_login_status($this->login_user['id'])) {
            redirect('home');
        }
    }

    /**
     *
     */
    public function index()
    {
        $this->load->view('vw_meetings');
    }

    /**
     *
     */
    public function view($meeting_id = null)
    {
        $this->load->view('vw_meeting_detail');
    }

    /**
     * Add meeting
     */
    public function add($meeting_id = null)
    {
        //this for meeting add//


        if ($this->input->post('meeting')) {
            // echo '<pre>' . var_export($_POST, true) . '<pre>';
//
//            $transactions_categories = array();
//
//            foreach($_POST['supplier'] as $row)
//            {
//                $transactions_categories[] = array('category_id_fk'=>$row['category']);
//
//            }
//            var_dump($transactions_categories);
            //$trans_count= count($_POST['supplier'])-1;
            //exit;
            //input array working =============================
            $input_array = array(
                'name' => $this->input->post('meeting_name'),
                'datetime' => $this->input->post('meeting_date'),
                'user_id_fk' => $this->login_user['id'],

            );
            $category_array = array(
                'meeting_type' => $this->input->post('meeting_type'),
                'meeting_location' => $this->input->post('meeting_location')
            );
            $transction_array = array();
            $transactions_categories = array();
            foreach ($_POST['supplier'] as $row) {
//
                $transction_array[] = array('description' => $row['desc'], 'amount' => $row['amount'], 'is_delivered' => $row['delivery'], 'is_advance' => $row['advance'], 'supplier_id_fk' => $row['id']);
                foreach ($row['category'] as $cat) {
                    $transactions_categories[] = array('category_data_id_fk' => $cat);
                }
            }


            //var_dump($_POST['supplier']);

            $insert_id = $this->MeetingModel->insert($input_array, $category_array, $transction_array, $transactions_categories, array());
            redirect('/meeting/listing/', 'refresh');

            //================================================================

        }
//=========================================
        $this->load->view('vw_new_add_meeting');
    }

    /**
     * edit meeting
     */
    public function edit($meeting_id = null)
    {
        $this->load->view('vw_meeting_settings');
    }

    /**
     * export meeting
     */
    public function export($meeting_id = null)
    {

    }

    public function listing($meeting_id = null)
    {

        $this->load->model('MeetingModel');
        $data['meeting'] = $this->MeetingModel->getall($this->login_user['id']);
        $this->load->view('vm_meeting_listing', $data);
    }

    /* public function total(){
         $this->load->view('vw_total_meetings');
     }*/
}