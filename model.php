


<!-- THEME DEBUG -->
<!-- CALL: theme('html') -->
<!-- FILE NAME SUGGESTIONS:
   * html--user.tpl.php
   x html.tpl.php
-->
<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/html.tpl.php' -->
<!doctype html>
<html>
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="shortcut icon" href="http://test.drupalserver.com/misc/favicon.ico" type="image/vnd.microsoft.icon" />
    <meta name="Generator" content="Drupal 7 (http://drupal.org)" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> User account | test.drupalserver.com </title>
    <style type="text/css" media="all">
        @import url("http://test.drupalserver.com/modules/system/system.base.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/system/system.menus.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/system/system.messages.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/system/system.theme.css?o2u57v");
    </style>
    <style type="text/css" media="all">
        @import url("http://test.drupalserver.com/modules/comment/comment.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/field/theme/field.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/node/node.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/search/search.css?o2u57v");
        @import url("http://test.drupalserver.com/modules/user/user.css?o2u57v");
    </style>
    <style type="text/css" media="all">
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/modules/ctools/css/ctools.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/modules/hybridauth/plugins/icon_pack/hybridauth_16/hybridauth_16.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/modules/hybridauth/css/hybridauth.modal.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/modules/hybridauth/css/hybridauth.css?o2u57v");
    </style>
    <style type="text/css" media="all">
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/bootstrap.min.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/calendar.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/bootstrap-datetimepicker.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/component.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/custom_2.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/font-awesome.min.css?o2u57v");
        @import url("http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/css/style.css?o2u57v");
    </style>
    <script type="text/javascript" src="http://test.drupalserver.com/misc/jquery.js?v=1.4.4"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/misc/jquery.once.js?v=1.2"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/misc/drupal.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/misc/jquery.cookie.js?v=1.0"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/modules/hybridauth/js/hybridauth.onclick.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/modules/hybridauth/js/hybridauth.modal.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/jquery.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/bootstrap.min.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/data.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/jquery.calendario.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/bootstrap-datetimepicker.js?o2u57v"></script>
    <script type="text/javascript" src="http://test.drupalserver.com/sites/test.drupalserver.com/themes/emanager/js/modernizr.custom.63321.js?o2u57v"></script>
    <script type="text/javascript">
        <!--//--><![CDATA[//><!--
        jQuery.extend(Drupal.settings, {"basePath":"\/","pathPrefix":"","ajaxPageState":{"theme":"emanager","theme_token":"x0F9R3Nev0muvtBWOjw9CCqx1cH1iE2NEkAH6Kr0f1E","js":{"misc\/jquery.js":1,"misc\/jquery.once.js":1,"misc\/drupal.js":1,"misc\/jquery.cookie.js":1,"sites\/test.drupalserver.com\/modules\/hybridauth\/js\/hybridauth.onclick.js":1,"sites\/test.drupalserver.com\/modules\/hybridauth\/js\/hybridauth.modal.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/jquery.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/bootstrap.min.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/data.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/jquery.calendario.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/bootstrap-datetimepicker.js":1,"sites\/test.drupalserver.com\/themes\/emanager\/js\/modernizr.custom.63321.js":1},"css":{"modules\/system\/system.base.css":1,"modules\/system\/system.menus.css":1,"modules\/system\/system.messages.css":1,"modules\/system\/system.theme.css":1,"modules\/comment\/comment.css":1,"modules\/field\/theme\/field.css":1,"modules\/node\/node.css":1,"modules\/search\/search.css":1,"modules\/user\/user.css":1,"sites\/test.drupalserver.com\/modules\/ctools\/css\/ctools.css":1,"sites\/test.drupalserver.com\/modules\/hybridauth\/plugins\/icon_pack\/hybridauth_16\/hybridauth_16.css":1,"sites\/test.drupalserver.com\/modules\/hybridauth\/css\/hybridauth.modal.css":1,"sites\/test.drupalserver.com\/modules\/hybridauth\/css\/hybridauth.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/bootstrap.min.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/calendar.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/bootstrap-datetimepicker.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/component.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/custom_2.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/font-awesome.min.css":1,"sites\/test.drupalserver.com\/themes\/emanager\/css\/style.css":1}}});
        //--><!]]>
    </script>
</head>
<!--  /var/www/vhosts/drupal_server/sites/test.drupalserver.com/themes/emanager/templates/html.tpl.php -->



<!-- THEME DEBUG -->
<!-- CALL: theme('page') -->
<!-- FILE NAME SUGGESTIONS:
   x page--user.tpl.php
   * page.tpl.php
-->
<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/page--user.tpl.php' -->
<!-- /var/www/vhosts/drupal_server/sites/test.drupalserver.com/themes/emanager/templates/page--user.tpl.php -->
<body >

?
<!-- page top if any is there possibly contenting admin bar -->
<!-- system navigation menus -->
<!-- exp-header -->
<header class="container-fluid ">
    <div class="row">
        <div class="container">
            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-6">
                <!-- logo -->
        <span class="exp-logo">
          Expense<span class="txt-cyan">Manager</span>
        </span>
            </div>
            <!-- menu block starts -->
            <div class="col-lg-9 col-md-8 col-sm-7 col-xs-6 exp-menu hidden-xs" >
                <ul class="cl-effect-1">
                    <li><a href="#" id="login" type="button" data-toggle="modal" data-target="#signin"><i class="fa fa-user"></i> Login</a></li>
                    <li><a href="#" type="button" type="button" data-toggle="modal" data-target="#signup" ><i class="fa fa-user"></i> Register</a></li>
                </ul>
            </div>
        </div>
</header>

<!-- proccing header naviagetion and user manu -->


<!-- $messages for message-->

<!--Alert Bar-->
<div class="container-fluid">
    <div class="row">
        <div class="container">
            <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:20px;">
                <div class="messages status">
                    <h2 class="element-invisible">Status message</h2>
                    <ul>
                        <li>Thank you for verifing your e-mail address.</li>
                        <li>Your account has been verified. Now you may login you account.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>





<!-- THEME DEBUG -->
<!-- CALL: theme('region') -->
<!-- FILE NAME SUGGESTIONS:
   * region--content.tpl.php
   x region.tpl.php
-->
<!-- BEGIN OUTPUT from 'modules/system/region.tpl.php' -->
<div class="region region-content">


    <!-- THEME DEBUG -->
    <!-- CALL: theme('block') -->
    <!-- FILE NAME SUGGESTIONS:
       * block--system--main.tpl.php
       * block--system.tpl.php
       * block--content.tpl.php
       x block.tpl.php
    -->
    <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/block.tpl.php' -->
    <div id="block-system-main" class="block block-system" style="padding-top:15px">


        <div class="content">
            <form action="/user" method="post" id="user-login" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
                        <label for="edit-name">Username <span class="form-required" title="This field is required.">*</span></label>
                        <input type="text" id="edit-name" name="name" value="" size="60" maxlength="60" class="form-text required" />
                        <div class="description">Enter your test.drupalserver.com username.</div>
                    </div>
                    <div class="form-item form-type-password form-item-pass">
                        <label for="edit-pass">Password <span class="form-required" title="This field is required.">*</span></label>
                        <input type="password" id="edit-pass" name="pass" size="60" maxlength="128" class="form-text required" />
                        <div class="description">Enter the password that accompanies your username.</div>
                    </div>
                    <input type="hidden" name="form_build_id" value="form-S6DNjweOHRhaAPIDv15WKrwy3W3_aJFaYQws6_F1gc8" />
                    <input type="hidden" name="form_id" value="user_login" />
                    <div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Log in" class="form-submit" /></div></div></form>  </div>
    </div>

    <!-- END OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/block.tpl.php' -->

</div>

<!-- END OUTPUT from 'modules/system/region.tpl.php' -->




<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="signin">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sign in</h4>
            </div>
            <div class="modal-body">
                <form action="/user" method="post" id="user-login--2" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-name">
                            <label for="edit-name--2">Username <span class="form-required" title="This field is required.">*</span></label>
                            <input type="text" id="edit-name--2" name="name" value="" size="29" maxlength="60" class="form-text required" />
                            <div class="description">Enter your test.drupalserver.com username.</div>
                        </div>
                        <div class="form-item form-type-password form-item-pass">
                            <label for="edit-pass--2">Password <span class="form-required" title="This field is required.">*</span></label>
                            <input type="password" id="edit-pass--2" name="pass" size="30" maxlength="128" class="form-text required" />
                            <div class="description">Enter the password that accompanies your username.</div>
                        </div>
                        <input type="hidden" name="form_build_id" value="form-64kgHAUt-74QltaWfM6XMGThC8zuizvAqWchdtGKXbY" />
                        <input type="hidden" name="form_id" value="user_login" />
                        <div class="form-actions form-wrapper" id="edit-actions--2"><input type="submit" id="edit-submit--2" name="op" value="Log in" class="form-submit" /></div><div class="login-title"><span>Login With</span></div>

                        <!-- THEME DEBUG -->
                        <!-- CALL: theme('hybridauth_widget') -->
                        <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->
                        <div class="hybridauth-widget-wrapper"><div class="item-list"><ul class="hybridauth-widget"><li class="first"><a href="/user?destination=/user&amp;destination_error=user" title="Facebook" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Facebook" data-hybridauth-url="/hybridauth/window/Facebook?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

                                            <!-- THEME DEBUG -->
                                            <!-- CALL: theme('hybridauth_provider_icon') -->
                                            <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
                                            <span class="hybridauth-icon facebook hybridauth-icon-hybridauth-16 hybridauth-facebook hybridauth-facebook-hybridauth-16" title="Facebook"></span>
                                            <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

                                        </a></li>
                                    <li class="last"><a href="/user?destination=/user&amp;destination_error=user" title="Twitter" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Twitter" data-hybridauth-url="/hybridauth/window/Twitter?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

                                            <!-- THEME DEBUG -->
                                            <!-- CALL: theme('hybridauth_provider_icon') -->
                                            <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
                                            <span class="hybridauth-icon twitter hybridauth-icon-hybridauth-16 hybridauth-twitter hybridauth-twitter-hybridauth-16" title="Twitter"></span>
                                            <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

                                        </a></li>
                                </ul></div></div>
                        <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->

                    </div></form></div></div></div></div>
<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="signup">

    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Sign Up</h4>
            </div>
            <div class="modal-body">
                <form action="/user" method="post" id="-form-user-register--2" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-username">
                            <label for="edit-username--2">Username <span class="form-required" title="This field is required.">*</span></label>
                            <input class="pull-right form-text required" placeholder="Username" type="text" id="edit-username--2" name="username" value="" size="30" maxlength="255" />
                        </div>
                        <div class="form-item form-type-password form-item-password">
                            <label for="edit-password--2">Password <span class="form-required" title="This field is required.">*</span></label>
                            <input class="pull-right form-text required" placeholder="Password" type="password" id="edit-password--2" name="password" size="30" maxlength="255" />
                        </div>
                        <div class="form-item form-type-textfield form-item-email">
                            <label for="edit-email--2">Email <span class="form-required" title="This field is required.">*</span></label>
                            <input class="pull-right form-text required" placeholder="Email" type="text" id="edit-email--2" name="email" value="" size="30" maxlength="128" />
                        </div>
                        <input class="btn btn-info btn-xs pull-right form-submit" style="margin: 5px" type="submit" id="edit-submit--4" name="op" value="Submit" /><input type="hidden" name="form_build_id" value="form-6k_dx2ZkZPECLNHa0EC4k6749rsStwRb1Ux5SlLIYKg" />
                        <input type="hidden" name="form_id" value="_form_user_register" />
                        <div>Register With ....</div>

                        <!-- THEME DEBUG -->
                        <!-- CALL: theme('hybridauth_widget') -->
                        <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->
                        <div class="hybridauth-widget-wrapper"><div class="item-list"><ul class="hybridauth-widget"><li class="first"><a href="/user?destination=/user&amp;destination_error=user" title="Facebook" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Facebook" data-hybridauth-url="/hybridauth/window/Facebook?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

                                            <!-- THEME DEBUG -->
                                            <!-- CALL: theme('hybridauth_provider_icon') -->
                                            <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
                                            <span class="hybridauth-icon facebook hybridauth-icon-hybridauth-16 hybridauth-facebook hybridauth-facebook-hybridauth-16" title="Facebook"></span>
                                            <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

                                        </a></li>
                                    <li class="last"><a href="/user?destination=/user&amp;destination_error=user" title="Twitter" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Twitter" data-hybridauth-url="/hybridauth/window/Twitter?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

                                            <!-- THEME DEBUG -->
                                            <!-- CALL: theme('hybridauth_provider_icon') -->
                                            <!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
                                            <span class="hybridauth-icon twitter hybridauth-icon-hybridauth-16 hybridauth-twitter hybridauth-twitter-hybridauth-16" title="Twitter"></span>
                                            <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

                                        </a></li>
                                </ul></div></div>
                        <!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->

                    </div></form></div></div></div></div>
<script>
    jQuery('.datetime').datepicker({
        format:'yyyy-m-d'
    });
</script>

</body>

<!-- END OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/page--user.tpl.php' -->


</html>

<!-- END OUTPUT from 'sites/test.drupalserver.com/themes/emanager/templates/html.tpl.php' -->

