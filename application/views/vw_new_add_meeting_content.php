<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add Meeting</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent; min-height:72.3vh;">
  <div class="row">
    <div class="container">
     <form class="form-horizontal">
      <!--<div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" style="padding-top:15px;" >
            <!--<h3 class="txt-blue"> Profile</h3>
           
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="meetingName" placeholder="enter meeting name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Type :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Meeting</option>
                          <option>Meeting Type</option>
                          <option>Meeting Type</option>
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Location :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Location</option>
                          <option>Meeting Location</option>
                          <option>Meeting Location</option>
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Date :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control datepicker" name="meetingDate" placeholder="enter meeting date" required>
                </div>
                <div class="clearfix"></div>
              </div>
              
              
              <div class="clearfix"></div>
            
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="height:440px ; padding:5px;;">
            
            <img src="images/meeting.png" alt="seller" style="width:100% ; height:100%;"/>
            <div class="clearfix"></div>
          </div>
          
        </div>
        <div class="clearfix"></div>
      </div>-->
      <div class="row" style="margin-bottom:30px;">
      		<div class="col-lg-12" style="margin-bottom:5px;"><h4> Meeting</h4></div>
      		<div class="col-lg-12 div-table">
            	<div class="div-row bg-blue bg-gray">
                	<div class="div-cell">Meeting Name</div>
                    <div class="div-cell">Meeting Type </div>
                    <div class="div-cell">Meeting Location </div>
                    <div class="div-cell">Meeting Date</div>
                    
                </div>
                <div class="div-row bg-white">
                	<div class="div-cell"><input type="text" name="companyName"/></div>
                    <div class="div-cell">
                       <label>
                            <select>
                              <option selected disabled> Select Meeting</option>
                              <option>Meeting Type</option>
                              <option>Meeting Type</option>
                            </select>
                       </label>
                    </div>
                    <div class="div-cell">
                    	 <label>
                            <select>
                              <option selected disabled> Select Location</option>
                              <option>Meeting Location</option>
                              <option>Meeting Location</option>
                            </select>
                   		</label>
                    </div>
                    <div class="div-cell">
                    	<input type="text" class="form-control exp-datepicker" name="meetingDate" placeholder="enter meeting date" required>
                    </div>
                  
                </div>
            </div>
      </div>
      
      <div class="row">
      		<div class="col-lg-12" style="margin-bottom:5px;"><h4> Suppliers</h4></div>
      		<div class="col-lg-12 div-table">
            	<div class="div-row bg-blue bg-gray">
                	<div class="div-cell"  style="width:3%; text-align:center;"> #</div>
                	<div class="div-cell"> Company Name</div>
                    <div class="div-cell">Contact F-Name </div>
                    <div class="div-cell">Contact L-Name </div>
                    <div class="div-cell">Address</div>
                    <div class="div-cell"> Country</div>
                    <div class="div-cell"> City</div>
                    <div class="div-cell"> Email</div>
                    <div class="div-cell"> Website</div>
                    <div class="div-cell"> Twitter ID</div>
                    <div class="div-cell"> Tel. NO.</div>
                    <div class="div-cell"> Save/ Edit</div>
                </div>
                <div class="div-row bg-white">
                	<div class="div-cell" style="width:3%; text-align:center;"><input type="checkbox"/></div>
                	<div class="div-cell"><input type="text" name="companyName"/></div>
                    <div class="div-cell"><input type="text" name="contactFname"/></div>
                    <div class="div-cell"><input type="text" name="contactLname"/></div>
                    <div class="div-cell"><input type="text" name="address"/></div>
                    <div class="div-cell"><input type="text" name="country"/></div>
                    <div class="div-cell"><input type="text" name="city" /></div>
                    <div class="div-cell"><input type="email" name="email"/></div>
                    <div class="div-cell"><input type="text" name="webSite"/></div>
                    <div class="div-cell"><input type="email" name="twitterID"/></div>
                    <div class="div-cell"><input type="tel" name="telNo"/></div>
                    <div class="div-cell"> 
                    	<button class="btn btn-xs btn-success" type="button"><i class="fa fa-plus"></i> Add</button>
                        
                    </div>
                </div>
            </div>
      </div>
      <div class="row" style="margin-top:20px;">
      		<div class="col-lg-12 text-right">
            	
                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-save"></i> Save</button>
                
              
            </div>
      </div>
      </form>
    </div>
  </div>
</div>