<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add seller</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" >
            <h3 class="txt-blue"> Profile</h3>
            <form class="form-horizontal">
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Company Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="companyName" placeholder="enter company name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact F-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contactFname" placeholder="enter first name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact L-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contactLname" placeholder="enter last name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Address :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="address" placeholder="enter address" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Country :</label>
                </div>
                <div class="col-lg-3">
                  <input type="text" class="form-control " name="country" placeholder="country name" required>
                </div>
                <div class="col-lg-2">
                  <label class="control-label"> City :</label>
                </div>
                <div class="col-lg-3">
                  <input type="text" class="form-control " name="city" placeholder="city name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Email :</label>
                </div>
                <div class="col-lg-8">
                  <input type="email" class="form-control " name="email" placeholder="enter email ID" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Tel No. :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="mobileNumber" placeholder="enter phone no" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Website Address :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="websiteAddress" placeholder="enter website " required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Twitter ID :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="twitterID" placeholder="enter twitter ID " required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="col-lg-12 text-right" >
                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                
              </div>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="height:68.5vh ; padding:5px;;">
            
            <img src="<?php echo ASSEST_PATH; ?>images/seller.jpg" alt="seller" style="width:100% ; height:100%;"/>
            <div class="clearfix"></div>
          </div>
          
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>