<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class User extends MY_Controller
{
    public $user_data;
    public function __construct()
    {
        parent::__construct();
        //check for user login



    }

    /**
     *
     */
    public function index(){
        redirect('user/profile');
    }

    public function profile($user_id = null){
        if(!check_login_status($user_id, true)){
            redirect('home');
        }
        $data = array();
        $this->user_data = get_user_record($user_id);
        if(isset($_POST['edit_user'])){
            $this->form_validation->set_rules('f_name', 'Contact First Name','max_length[50]');
            $this->form_validation->set_rules('l_name', 'Contact Last Name','max_length[50]');
            $this->form_validation->set_rules('address', 'Address','max_length[500]');
            $this->form_validation->set_rules('city', 'City','max_length[50]');
            $this->form_validation->set_rules('country', 'Country','max_length[50]');
            $this->form_validation->set_rules('mobile_no', 'Tel No.','max_length[12]');
            $this->form_validation->set_rules('website', 'Website Address','max_length[50]');
            $this->form_validation->set_rules('twitter_id', 'Twitter ID','max_length[50]');

            if($this->form_validation->run() == true){
                $array = array(
                    'f_name' => $this->input->post('f_name'),
                    'l_name' => $this->input->post('l_name'),
                    'address' => $this->input->post('address'),
                    'city' => $this->input->post('city'),
                    'country' => $this->input->post('country'),
                    'mobile_no' => $this->input->post('mobile_no'),
                    'website' => $this->input->post('website'),
                    'twitter_id' => $this->input->post('twitter_id'),
                );

                $this->db->where('ID', $user_id);
                $result = $this->db->update('users', $array);
                if($result){
                    $this->session->set_flashdata('success', 'Your profile edited successfully');
                    redirect('user/profile/'.$user_id);
                }

            }else{
               $data['error'] = true;
            }
        }
        $this->load->view('vw_company_profile', $data);

    }


    public function add(){
       //$this->load->view('vw_new_add_meeting');
    }
    /**
     * Edit user
     */
    public function edit($user_id){
        if(!check_login_status($user_id, true)){
            redirect('home');
        }
        $this->user_data = get_user_record($this->login_user['id']);

     }
}