<footer class="container-fluid exp-footer ">
  <div class="row">
    <div class="container">
      <div class="col-lg-6"> <span>Provided by</span> <b>CoreTechies</b> </div>
      <div class="col-lg-6">
        <ul>
          <li><a href="#">Settings</a></li>
          <li><a href="#"><i class="fa fa-question-circle"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
</footer>
<script src="<?php echo ASSEST_PATH; ?>js/jquery.js"></script> 
<script src="<?php echo ASSEST_PATH; ?>js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo ASSEST_PATH; ?>js/jquery.calendario.js"></script> 
<script src="<?php echo ASSEST_PATH; ?>js/modernizr.custom.63321.js"></script> 
<script type="text/javascript" src="<?php echo ASSEST_PATH; ?>js/data.js"></script> 
<script src="<?php echo ASSEST_PATH; ?>js/jquery.mCustomScrollbar.concat.min.js"></script> 
<script src="<?php echo ASSEST_PATH; ?>js/jquery.dataTables.min.js"></script>
<!--<script src="<?php /*echo ASSEST_PATH; */?>bootstrap3-editable/js/bootstrap-editable.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php /*echo ASSEST_PATH; */?>bootstrap3-editable/css/bootstrap-editable.css"/>
-->



<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="signup">

	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Sign Up</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="<?php echo base_url() ?>home/registration" accept-charset="UTF-8">
					<div>
						<div class="form-item form-type-textfield form-item-username">
							<label for="edit-username--2">Username <span class="form-required" title="This field is required.">*</span></label>
							<input class="pull-right form-text required" placeholder="Username" type="text" name="username" value="<?php echo set_value('username'); ?>" size="30" maxlength="50" />
							<span class="error"><?php echo form_error('username') ?></span>
						</div>
						<div class="form-item form-type-password form-item-password">
							<label for="edit-password--2">Password <span class="form-required" title="This field is required.">*</span></label>
							<input class="pull-right form-text required" placeholder="Password" type="password" name="password" size="30" maxlength="50" />
							<span class="error"><?php echo form_error('password') ?></span>
						</div>
						<div class="form-item form-type-textfield form-item-email">
							<label for="edit-email--2">Email <span class="form-required" title="This field is required.">*</span></label>
							<input class="pull-right form-text required" placeholder="Email" type="text" name="email" value="<?php echo set_value('email'); ?>" size="30" maxlength="128" />
							<span class="error"><?php echo form_error('email') ?></span>
						</div>
						<input class="btn btn-info btn-xs pull-right form-submit" style="margin: 5px" type="submit" name="registration" value="Submit" />
						<!--<input type="hidden" name="form_build_id" value="form-6k_dx2ZkZPECLNHa0EC4k6749rsStwRb1Ux5SlLIYKg" />
                        <input type="hidden" name="form_id" value="_form_user_register" />-->

						<div>Register With ....</div>

						<!-- THEME DEBUG -->
						<!-- CALL: theme('hybridauth_widget') -->
						<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->
						<div class="hybridauth-widget-wrapper">
							<div class="item-list">
								<ul class="hybridauth-widget">
									<li class="first">
										<a href="/user?destination=/user&amp;destination_error=user" title="Facebook" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Facebook" data-hybridauth-url="/hybridauth/window/Facebook?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

											<!-- THEME DEBUG -->
											<!-- CALL: theme('hybridauth_provider_icon') -->
											<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
											<span class="hybridauth-icon facebook hybridauth-icon-hybridauth-16 hybridauth-facebook hybridauth-facebook-hybridauth-16" title="Facebook"></span>
											<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

										</a>
									</li>
									<li class="last">
										<a href="/user?destination=/user&amp;destination_error=user" title="Twitter" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Twitter" data-hybridauth-url="/hybridauth/window/Twitter?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

											<!-- THEME DEBUG -->
											<!-- CALL: theme('hybridauth_provider_icon') -->
											<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
											<span class="hybridauth-icon twitter hybridauth-icon-hybridauth-16 hybridauth-twitter hybridauth-twitter-hybridauth-16" title="Twitter"></span>
											<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->

					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id="signin">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Sign in</h4>
			</div>
			<div class="modal-body">
				<form action="<?php echo base_url() ?>login" method="post" accept-charset="UTF-8">
					<div>
						<div class="form-item form-type-textfield form-item-name">
							<label for="edit-name--2">Username <span class="form-required" title="This field is required.">*</span></label>
							<input class="pull-right form-text required" placeholder="Username" type="text" name="username" value="<?php echo set_value('username'); ?>" size="30" maxlength="50" />
							<span class="error"><?php echo form_error('username') ?></span>
						</div>
						<div class="form-item form-type-password form-item-pass">
							<label for="edit-pass--2">Password <span class="form-required" title="This field is required.">*</span></label>
							<input class="pull-right form-text required" placeholder="Password" type="password" name="password" size="30" maxlength="50" />
							<span class="error"><?php echo form_error('password') ?></span>
						</div>
						<div class="form-item form-type-password form-item-pass">
							<span class="error"><?php if(isset($message)) echo $message; ?></span>
						</div>
						<input type="hidden" name="form_build_id" value="form-64kgHAUt-74QltaWfM6XMGThC8zuizvAqWchdtGKXbY" />
						<input type="hidden" name="form_id" value="user_login" />
						<div class="form-actions form-wrapper" >
							<input type="submit" id="edit-submit--2" name="login_submit" value="Log in" class="form-submit" />
						</div>
						<div class="login-title"><span>Login With</span></div>

						<!-- THEME DEBUG -->
						<!-- CALL: theme('hybridauth_widget') -->
						<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->
						<div class="hybridauth-widget-wrapper">
							<div class="item-list">
								<ul class="hybridauth-widget">
									<li class="first">
										<a href="/user?destination=/user&amp;destination_error=user" title="Facebook" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Facebook" data-hybridauth-url="/hybridauth/window/Facebook?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

											<!-- THEME DEBUG -->
											<!-- CALL: theme('hybridauth_provider_icon') -->
											<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
											<span class="hybridauth-icon facebook hybridauth-icon-hybridauth-16 hybridauth-facebook hybridauth-facebook-hybridauth-16" title="Facebook"></span>
											<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

										</a>
									</li>
									<li class="last">
										<a href="/user?destination=/user&amp;destination_error=user" title="Twitter" class="hybridauth-widget-provider hybridauth-onclick-popup active" rel="nofollow" data-hybridauth-provider="Twitter" data-hybridauth-url="/hybridauth/window/Twitter?destination=/user&amp;destination_error=user" data-ajax="false" data-hybridauth-width="800" data-hybridauth-height="500">

											<!-- THEME DEBUG -->
											<!-- CALL: theme('hybridauth_provider_icon') -->
											<!-- BEGIN OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->
											<span class="hybridauth-icon twitter hybridauth-icon-hybridauth-16 hybridauth-twitter hybridauth-twitter-hybridauth-16" title="Twitter"></span>
											<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_provider_icon.tpl.php' -->

										</a>
									</li>
								</ul>
							</div>
						</div>
						<!-- END OUTPUT from 'sites/test.drupalserver.com/modules/hybridauth/templates/hybridauth_widget.tpl.php' -->

					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<?php
if(isset($registration_fail)){ ?>
	<script>
		$(document).ready(function(){
			$('#signup').modal('toggle');
			$('#signup').modal('show');
		});
	</script>
<?php }
?>

<?php
if(isset($login_fail)){ ?>
	<script>
		$(document).ready(function(){
			$('#signin').modal('toggle');
			$('#signin').modal('show');
		});
	</script>
<?php }
?>
<script>
	$(document).ready(function(){
		$('#edit_user').click(function(){
			$('.display_data').css('display', 'none');
			$('.display_input_data').css('display', 'block');
		});
	});
</script>

<script>
  (function($){
			$(window).load(function(){
				
				$(".scroll").mCustomScrollbar({
					autoHideScrollbar:true,
					theme:"rounded"
				});
				
			});
		})(jQuery);  
</script> 
<script>
        	$(document).ready(function() {

			$('.dataTable').DataTable();
			
		} );
</script> 
<script>
$(document).ready(function(){
    var t = $('.dataTable').DataTable();
    var counter = "";
 
   $(document).on("click",".addrow",function() {
        t.row.add( [
            counter +'<input type="text"/>',
            counter +'<input type="text"/>',
            counter +'<input type="text"/>',
            counter +'<input type="text"/>',
            counter +'<input type="text"/>',
			counter +'<input type="text"/>',
			counter +'<input type="text"/>',
			counter +'<input type="text"/>',
			counter +'<button class="btn btn-xs btn-success"><i class="fa fa-save"></i> Save</button>'
        ] ).draw( false );
 
       
    } );
 
  
  
} );
</script> 
<script type="text/javascript">	
			$(function() {
			
				var transEndEventNames = {
						'WebkitTransition' : 'webkitTransitionEnd',
						'MozTransition' : 'transitionend',
						'OTransition' : 'oTransitionEnd',
						'msTransition' : 'MSTransitionEnd',
						'transition' : 'transitionend'
					},
					transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
					$wrapper = $( '#custom-inner' ),
					$calendar = $( '#calendar' ),
					cal = $calendar.calendario( {
						onDayClick : function( $el, $contentEl, dateProperties ) {

							if( $contentEl.length > 0 ) {
								showEvents( $contentEl, dateProperties );
							}

						},
						caldata : codropsEvents,
						displayWeekAbbr : true
					} ),
					$month = $( '#custom-month' ).html( cal.getMonthName() ),
					$year = $( '#custom-year' ).html( cal.getYear() );

				$( '#custom-next' ).on( 'click', function() {
					cal.gotoNextMonth( updateMonthYear );
				} );
				$( '#custom-prev' ).on( 'click', function() {
					cal.gotoPreviousMonth( updateMonthYear );
				} );

				function updateMonthYear() {				
					$month.html( cal.getMonthName() );
					$year.html( cal.getYear() );
				}

				// just an example..
				function showEvents( $contentEl, dateProperties ) {

					hideEvents();
					
					var $events = $( '<div id="custom-content-reveal" class="custom-content-reveal"><h4>Events for ' + dateProperties.monthname + ' ' + dateProperties.day + ', ' + dateProperties.year + '</h4></div>' ),
						$close = $( '<span class="custom-content-close"></span>' ).on( 'click', hideEvents );

					$events.append( $contentEl.html() , $close ).insertAfter( $wrapper );
					
					setTimeout( function() {
						$events.css( 'top', '0%' );
					}, 25 );

				}
				function hideEvents() {

					var $events = $( '#custom-content-reveal' );
					if( $events.length > 0 ) {
						
						$events.css( 'top', '100%' );
						Modernizr.csstransitions ? $events.on( transEndEventName, function() { $( this ).remove(); } ) : $events.remove();

					}

				}
			
			});
		</script> 
<script>
$(document).ready(function() {
	$('button.edit_category').click(function(){
		var id = $(this).attr('id');
		if(id && id.length>0){
			$('#input_'+id).css('display', 'inline-block');
			$('#submit_'+id).css('display', 'inline-block');
			$('#cancel_'+id).css('display', 'inline-block');
			$('#delete_'+id).css('display', 'none');
			$('#value_'+id).css('display', 'none');
			$(this).css('display', 'none');
		}
	});
    //$('.editable').editable();
});
        </script> 
<script>
$(function(){
    /*$('#accountType').editable({
        value: 2,    
        source: [
              {value: 1, text: 'Premium'},
              {value: 2, text: 'Standard'}
             
           ]
    });*/


});
</script>
</body>
</html>