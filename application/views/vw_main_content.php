<div class="container-fluid exp-slider">
  <div class="row">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active"> <img src="images/banner.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="images/banner1.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="images/banner.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
        <div class="item"> <img src="images/banner1.jpg" alt="...">
          <div class="carousel-caption"> </div>
        </div>
      </div>
      
      <!-- Controls --> 
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
  </div>
</div>

<!-- Welcome Container-->
<div class="container-fluid exp-main-container">
  <div class="row">
    <div class="container">
      <h1 class="text-capitalize text-center">Welcome</h1>
      <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vestibulum molestie lacus. Aenean onummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis di arturient ontes nascetur ridiculus mus. Nulla du.</p>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      	<div class="col-lg-8 null-padding">
      	<div class="text-center">
        	<h1 class="exp-title-1">Events</h1>
        </div>
        
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla.  </p>
            </div>
          </li>
          
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
					<div id="custom-inner" class="custom-inner">
						<div class="custom-header clearfix">
							<nav>
								<span id="custom-prev" class="custom-prev"></span>
								<span id="custom-next" class="custom-next"></span>
							</nav>
							<h2 id="custom-month" class="custom-month"></h2>
							<h3 id="custom-year" class="custom-year"></h3>
						</div>
						<div id="calendar" class="fc-calendar-container"></div>
					</div>
				</div>
      </div>
      
    </div>
  </div>
</div>


<!-- Service Container-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
      <h1 class="text-capitalize text-center">Our Services</h1>
      <div class="exp-service-containe">
        <div class="col-lg-6">
          <figure class="exp-image"> <img src="images/11.jpg" alt="premium"> </figure>
          <section class="exp-section">
            <div class="col-lg-6">
              <h4 class="txt-blue">About Premium Service</h4>
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vetie lacus. Aenean ummy heerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus magnis diturient ontes nascetur ridiculula. Ut enim ad minima veniam.</p>
            </div>
            <div class="col-lg-6">
              <h4 class="txt-blue">Premium Services</h4>
              <ul>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </section>
        </div>
        <div class="col-lg-6">
          <figure class="exp-image"> <img src="images/10.png" alt="standars"> </figure>
          <section class="exp-section">
            <div class="col-lg-6">
              <h4 class="txt-blue">About Standard Service</h4>
              <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit. Praesent vetie lacus. Aenean ummy heerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus magnis diturient ontes nascetur ridiculula. Ut enim ad minima veniam.</p>
            </div>
            <div class="col-lg-6">
              <h4 class="txt-blue">Standard Services</h4>
              <ul>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                <li><a href="#">Lorem ipsum dolor sit amet</a></li>
              </ul>
            </div>
            <div class="clearfix"></div>
          </section>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>