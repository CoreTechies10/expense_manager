<?php

/**
 * Created by PhpStorm.
 * User: hitesh
 * Date: 17/03/16
 * Time: 16:00
 */

/**
 * Class Categories_Data
 * @property CI_DB_active_record db
 */
class Categories_Data extends CI_Model {

  protected static $table = 'categories_data';
  protected static $sub_tables = [];

  public function __construct() {
    parent::__construct();
    //setting sub table
    //    $this->load->model('categories');
    //    self::$sub_table = $this->categories->getTable();
    self::$sub_tables[ 'meeting_category' ] = 'meeting_category';
    self::$sub_tables[ 'categories_type' ] = 'categories_type';
    self::$sub_tables[ 'transactions_category' ] = 'transactions_category';
  }

  public function getTable() {
    return self::$table;
  }


  public function getSubTables($table = '') {
    return empty($table) ? self::$sub_tables : self::$sub_tables[ $table ];
  }

  public function getMeetingCategory($userId = NULL, $meeting_id = NULL) {
    if (empty($userId)) {
      $this->load->library('session');
      $userId = $this->session->userdata('ID');
      if ($userId === FALSE) {
        return array();
      }
    }
    $where = array('ct.asso_with' => 'MEETING', 'cd.user_id_fk' => $userId);
    if (!empty($meeting_id)) {
      $where[ 'mc.meeting_id_fk' ] = $meeting_id;
    }

    $this->db->join($this->getSubTables('meeting_category') . ' mc', 'cd.ID = mc.category_data_id_fk');
    $this->db->join($this->getSubTables('categories_type') . ' ct', 'cd.category_type_id_fk = ct.ID');

    $result = $this->db->get_where(self::$table . ' cd', $where);
    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();

    if (!empty($result)) {
      return $result;
    }
    return array();

  }

  public function getTransactionCategory(
    $userId = NULL,
    $meeting_id = NULL,
    $supplier_id = NULL,
    $transaction_id
    = NULL
  ) {
    if (empty($userId)) {
      $this->load->library('session');
      $userId = $this->session->userdata('ID');
      if ($userId === FALSE) {
        return array();
      }
    }
    $where = array('ct.asso_with' => 'TRANSACTION', 'cd.user_id_fk' => $userId);
    if (!empty($meeting_id)) {
      $where[ 't.meeting_id_fk' ] = $meeting_id;
    }
    if (!empty($supplier_id)) {
      $where[ 't.supplier_id_fk' ] = $supplier_id;
    }
    if (!empty($transaction_id)) {
      $where[ 'tc.transaction_id_fk' ] = $transaction_id;
    }

    $this->db->join($this->getSubTables('transactions_category') . ' tc', 'cd.ID = tc.category_data_id_fk');
    $this->db->join($this->getSubTables('categories_type') . ' ct', 'cd.category_type_id_fk = ct.ID');
    $this->load->model('transactionsmodel');

    $this->db->join($this->transactionsmodel->getTable() . ' t', 'tc.transaction_id_fk = t.ID');

    $result = $this->db->get_where(self::$table . ' cd', $where);

    //@var CI_DB_mysql_result $result ;
    $result = $result->result_array();

    if (!empty($result)) {
      return $result;
    }
    return array();

  }

}