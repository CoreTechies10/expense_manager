<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Transactions extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        //check for user login
    }

    /**
     *
     */
    public function index(){
        $this->load->model('TransactionsModel');
        $insert_ida['transction_list']=$this->TransactionsModel->TranscationListing('10','0');

        $this->load->view('vw_add_amount',$insert_ida);
    }

    /**
     * Add transaction
     */
    public function add(){

    }

    /**
     * edit Transaction
     */
    public function edit(){

    }

        public function send_value_ajax(){

        if($this->input->post('category_id'))
        {
            $message = array(
                'status' => false,
                'message' => ''
            );

            $category =$this->input->post('category_id');
            $payment =$this->input->post('payment_id');
            $company=$this->input->post("compny");
            $supplier=$this->input->post("supplier");
            $delivery=$this->input->post("delivery");
            $advance=$this->input->post("advance");
             $value=$this->input->post('value');
            header('content-type: application/json');
            $this->db->where('ID',$category);
            $this->db->or_where('ID',$payment);
            $this->db->select('value');
            $result = $this->db->get('categories_data');
            //for payment value
            //echo $this->db->last_query();
            if($result->num_rows>0)
            {

                $data=$result->result();
               // print_r($data);

                $latest_message='<tr><td>'.$company.'</td>';
                $latest_message.='<td>'.$supplier.'</td><td>'.$data[0]->value.'</td>';
                $latest_message.='<td>'.$this->input->post("amount_id").' </td>';
                $latest_message.='<td>'.$this->input->post("desc").' </td><td>'.$this->input->post("desc").' </td><td>'. $delivery.' </td>';
                $latest_message.='<td>'. $advance.' </td>';
                $message = array(
                    'status' => true,
                    'message'=>$latest_message
                );
            }


            else{

                $message['message'] = '';
            }

            echo json_encode($message);
        }
    }



}