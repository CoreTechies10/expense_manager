<?php
if(isset($error) && $error){
  ?>
    <style>
      .display_data{display: none}
      .display_input_data{display: block}
    </style>
  <?php
}else{
  ?>
  <style>
    .display_data{display: block}
    .display_input_data{display: none}
  </style>
  <?php
}
?>
<div class="container-fluid">
  <div class="row">
    <div class="container">
      <?php if($this->session->flashdata('success')): ?>
        <div class="alert alert-success alert-dismissable">

          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <?php echo $this->session->flashdata('success'); ?>
        </div>
      <?php endif; ?>
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Company Profile</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                <!--<td>
                                	<button class="btn btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>
                                </td>--> 
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Welcome Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
      <div class="col-lg-6 ">
          <div class=" exp-company-profile" >
            <h3 class="txt-blue"> Profile</h3>
            <form method="post" class="">
              <table class="table table-bordered">
                <tr>
                  <td>Company Name</td>
                  <td><?php echo $this->user_data['company_name'] ?></td>
                </tr>
                <tr>
                  <td>Contact First Name</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['f_name'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="First Name" type="text" name="f_name" value="<?php if(set_value('f_name')) echo set_value('f_name'); else echo $this->user_data['f_name']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('f_name') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Contact Last Name</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['l_name'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Last Name" type="text" name="l_name" value="<?php if(set_value('l_name')) echo set_value('l_name'); else echo $this->user_data['l_name']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('l_name') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['address'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <textarea maxlength="500" class="form-text required"  name="address"><?php if(set_value('address')) echo set_value('address'); else echo $this->user_data['address']; ?></textarea>
                     <span class="error"><?php echo form_error('address') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>City</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['city'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="city" type="text" name="city" value="<?php if(set_value('city')) echo set_value('city'); else echo $this->user_data['city']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('city') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Country</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['country'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="country" type="text" name="country" value="<?php if(set_value('country')) echo set_value('country'); else echo $this->user_data['country']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('country') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Tel No.</td>
                  <td>
                    <span class="display_data"><?php echo $this->user_data['mobile_no'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Tel No." type="text" name="mobile_no" value="<?php if(set_value('mobile_no')) echo set_value('mobile_no'); else echo $this->user_data['mobile_no']; ?>" size="30" maxlength="12" />
                      <span class="error"><?php echo form_error('mobile_no') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Website Address</td>
                  <td> </a>
                    <span class="display_data"><a href="#"><?php echo $this->user_data['website'] ?></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Website" type="text" name="website" value="<?php if(set_value('website')) echo set_value('website'); else echo $this->user_data['website']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('website') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Twitter ID</td>
                  <td>
                    <span class="display_data"><a href="#"><?php echo $this->user_data['twitter_id'] ?></a></span>
                    <div class="display_input_data form-item form-type-textfield form-item-name">
                      <input class="form-text required" placeholder="Twitter ID" type="text" name="twitter_id" value="<?php if(set_value('twitter_id')) echo set_value('twitter_id'); else echo $this->user_data['twitter_id']; ?>" size="30" maxlength="50" />
                      <span class="error"><?php echo form_error('twitter_id') ?></span>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Account Type</td>
                  <td>
                    <?php echo $this->user_data['type'];
                    if($this->user_data['type'] == 'Standard'):
                    ?>
                      <a href="<?php echo base_url().'test/buy'; ?>" class="btn btn-xs btn-warning">Go Premium</a>
                    <?php endif; ?>
                  </td>
                </tr>
              </table>
              <div class="col-md-2 pull-right text-right">
                <a id="edit_user" class="display_data btn btn-xs btn-info" ><i class="fa fa-edit"></i> Edit</a><!-- onclick="MyWindow=window.open('<?php echo base_url() ?>user/edit','update','scrollbars=yes,width=500,height=400,menubar=0,toolbar=0,location=0,directories=0,status=0,copyhistory=0'); return false;" -->
                <button name="edit_user" class="display_input_data btn btn-xs btn-info" ><i class="fa fa-edit"></i> Edit</button><!-- onclick="MyWindow=window.open('<?php echo base_url() ?>user/edit','update','scrollbars=yes,width=500,height=400,menubar=0,toolbar=0,location=0,directories=0,status=0,copyhistory=0'); return false;" -->

              </div>
            </form>


            
          </div>
          
        </div>
       
             
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="min-height:66vh;">
            <div >
              <div class="col-lg-6 null-padding" >
                <h3 class="txt-cyan"> Company Balance</h3>
              </div>
              <div class="col-lg-6 null-padding text-right" style="padding-top:10px;"> <span class="label label-danger txt-big">-$3700</span> </div>
              <div class="clearfix"></div>
            </div>
            <table class="table table-responsive table-bordered exp-table">
              <thead>
                <tr class=" bg-blue bg-gray">
                  <th></th>
                  <th>Total Paid</th>
                  <th>Total Delivery Value</th>
                  <th>Balance</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th >Food Supply</th>
                  <td>$2000</td>
                  <td>$500</td>
                  <td><span class="label label-success">$1500</span></td>
                </tr>
                <tr>
                  <th>Uniform Supply</th>
                  <td>$5000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$2000</span></td>
                </tr>
                <tr>
                  <th>Snacks</th>
                  <td>$1000</td>
                  <td>$200</td>
                  <td><span class="label label-success ">$800</span></td>
                </tr>
                <tr>
                  <th>Accessories</th>
                  <td>$2000</td>
                  <td>$1000</td>
                  <td><span class="label label-success ">$1000</span></td>
                </tr>
                <tr>
                  <th>Water</th>
                  <td>$3000</td>
                  <td>$7000</td>
                  <td><span class="label label-danger ">-$4000</span></td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="2"><span class="txt-big">Total Balance</span></td>
                  <td></td>
                  <td><span class="txt-big"><b>-$3700</b></span></td>
                </tr>
              </tfoot>
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>

<!-- Table Calander-->
<div class="container-fluid exp-main-container" >
  <div class="row">
    <div class="container">
      <div class="exp-tables">
        <header>
          <div class="col-lg-7">
            <h3><i class="fa fa-truck"></i> <span class="exp-title-1 txt-blue">Received Delivery Value </span></h3>
          </div>
          <div class="col-lg-5 text-right"> 
            <!--<button class="btn btn-default btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                        <button class="btn btn-default btn-xs btn-success"><i class="fa fa-edit"></i> Edit</button>--> 
          </div>
          <div class="clearfix"></div>
        </header>
        <table class="table table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Pay Type</th>
              <th>Desc</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$1000</td>
            </tr>
            <tr>
              <td>10-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Water</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$2000</td>
            </tr>
            <tr>
              <td>11-Sep-2015</td>
              <td>Nestle</td>
              <td>Robin(Rob)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cheque</td>
              <td>HSBC</td>
              <td>$1250</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <h3><i class="fa fa-money"></i> <span class="exp-title-1 txt-blue">Amount Paid in Advance</span></h3>
        <table class="table  table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Pay Type</th>
              <th>Desc</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$300</td>
            </tr>
            <tr>
              <td>10-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Water</td>
              <td>Cash</td>
              <td>Kam</td>
              <td>$300</td>
            </tr>
            <tr>
              <td>11-Sep-2015</td>
              <td>Nestle</td>
              <td>Robin(Rob)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>Cheque</td>
              <td>HSBC</td>
              <td>$200</td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="exp-tables">
        <h3><i class="fa fa-users"></i> <span class="exp-title-1 txt-blue">UpComming Meetings</span></h3>
        <table class="table table-bordered dataTable">
          <thead>
            <tr class=" bg-blue bg-gray">
              <th>Date</th>
              <th>Company Name</th>
              <th>Contact FnLname</th>
              <th>Meeting</th>
              <th>Catogry</th>
              <th>Location</th>
              <th>Amount</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>01-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>HIltonCD</td>
              <td>$300</td>
            </tr>
            <tr>
              <td>10-Sep-2015</td>
              <td>XYZ Pvt ltd</td>
              <td>Williams(Will)</td>
              <td>T2Project</td>
              <td>Water</td>
              <td>MariotBC</td>
              <td>$300</td>
            </tr>
            <tr>
              <td>11-Sep-2015</td>
              <td>Nestle</td>
              <td>Robin(Rob)</td>
              <td>T2Project</td>
              <td>Snacks</td>
              <td>HIltonCD</td>
              <td>$200</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Event Calander-->
<div class="container-fluid exp-main-container"  style="background-color: transparent;">
  <div class="row">
    <div class="container">
      <div class="col-lg-8 null-padding">
        <div class="text-center">
          <h1 class="exp-title-1">Events</h1>
        </div>
        <ul>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 10:20PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 01:00PM<span>
              <p> Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit. </p>
            </div>
          </li>
          <li class="media">
            <div class="media-left event-date"> <a href="#" > 16 <span>Sep</span> </a> </div>
            <div class="media-body">
              <h4 class="media-heading  txt-blue">Event Heading</h4>
              <span class="event-time"><i class="fa fa-clock-o "></i> 4:30PM<span>
              <p> Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. </p>
            </div>
          </li>
        </ul>
      </div>
      <div class="col-lg-4 null-padding">
        <div class="custom-calendar-wrap">
          <div id="custom-inner" class="custom-inner">
            <div class="custom-header clearfix">
              <nav> <span id="custom-prev" class="custom-prev"></span> <span id="custom-next" class="custom-next"></span> </nav>
              <h2 id="custom-month" class="custom-month"></h2>
              <h3 id="custom-year" class="custom-year"></h3>
            </div>
            <div id="calendar" class="fc-calendar-container"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>