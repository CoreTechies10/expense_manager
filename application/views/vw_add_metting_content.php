<div class="container-fluid">
  <div class="row">
    <div class="container">
      <div class="col-lg-12 exp-company-profile " style="margin-top:20px; margin-bottom:-20px;">
        <div class="row">
          <div class="col-lg-6">
            <h3 class="txt-cyan" style="margin-bottom:0px; line-height:1.7em;">Add Meeting</h3>
          </div>
          <div class="col-lg-3 col-lg-offset-3">
            <table class="table table-bordered filter-bar" style="margin-bottom:0px;">
              <tr>
                <td style=" line-height:2em;" > Company Names : </td>
                <td><button type="button" class="btn btn-default dropdown-toggle btn-xs btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i> <span class="caret"></span></button>
                  <ul class="dropdown-menu dropdown-menu-right company-list">
                    <li><a href="#" >AbcFood-Williams(Will)</a></li>
                    <li><a href="#">Nestle-Robbin (Rob)</a></li>
                    <li><a href="#">Volvic-Cameron (Cam)</a></li>
                  </ul></td>
                
              </tr>
            </table>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Main Container-->
<div class="container-fluid exp-main-container border-top0" style="background-color:transparent;">
  <div class="row">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 ">
          <div class=" exp-company-profile" style="padding-top:15px;" >
            <!--<h3 class="txt-blue"> Profile</h3>-->
            <form class="form-horizontal">
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="meetingName" placeholder="enter meeting name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Type :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Meeting</option>
                          <option>Meeting Type</option>
                          <option>Meeting Type</option>
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Location :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Location</option>
                          <option>Meeting Location</option>
                          <option>Meeting Location</option>
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Meeting Date :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control datepicker" name="meetingDate" placeholder="enter meeting date" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Company Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="companyName" placeholder="enter company name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact F-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contactFname" placeholder="enter seller first name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Contact L-Name :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="contactLname" placeholder="enter seller last name" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Catogry :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Catogry</option>
                          <option>Water</option>
                          <option>Snacks</option>
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Ammount :</label>
                </div>
                <div class="col-lg-8">
                  <input type="text" class="form-control " name="ammount" placeholder="enter ammount" required>
                </div>
                <div class="clearfix"></div>
              </div>
              <div class="form-group">
                <div class="col-lg-4">
                  <label class="control-label"> Payment Type :</label>
                </div>
                <div class="col-lg-8 exp-select">
                  <label>
                        <select>
                          <option selected disabled> Select Payment</option>
                          <option>Advance</option>
                          <option>Cash</option>
                          <option>Credit Card</option>
                          
                        </select>
                   </label>
                </div>
                <div class="clearfix"></div>
              </div>
              
              <div class="col-lg-12 text-right" >
                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Add</button>
                
              </div>
              <div class="clearfix"></div>
            </form>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="exp-company-profile"  style="height:440px ; padding:5px;;">
            
            <img src="images/meeting.png" alt="seller" style="width:100% ; height:100%;"/>
            <div class="clearfix"></div>
          </div>
          
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>