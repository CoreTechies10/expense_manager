<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


//namespace Login;

/**
 * Class Login
 *
 */
class Login extends MY_Controller
{
    public function __construct(){
        parent::__construct();
        $this->load->model('Users');
    }

    /**
     *
     **/
    public function index(){
        //check for user login
        if(check_login_status()){
            //redirect('home', 'refresh');
        }

        if(isset($_POST['login_submit'])){
            // set validation rule
            $this->form_validation->set_rules('username', 'User Name', 'required|max_length[50]');// for user name
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[50]');// for password

            if($this->form_validation->run() == true){
                // set login data
                $company_name = $this->input->post('username');
                $password = $this->input->post('password');

                //
                $result = $this->Users->identifyUser($company_name, $password);
                if($result){
                    if($result['confirmation_status'] == 1){
                        // redirect to after login process
                        $sess_data = array(
                            'company_name' => $result['company_name'],
                            'id' => $result['ID'],
                            'user_type' => $result['type'],
                        );

                        $this->session->set_userdata('login_user', $sess_data);
                        redirect('home');

                    }else{
                        // Show login error message
                        $data = array(
                            'login_fail' => true,
                            'message' => 'Sorry you are not confirmed, please click on the confirmation link send in you mail.'
                        );
                        //load login view
                        $this->load->view('vw_home', $data);
                    }

                }else{
                    // Show login error message
                    $data = array(
                        'login_fail' => true,
                        'message' => 'Invalid username and password.'
                    );
                    //load login view
                    $this->load->view('vw_home', $data);

                }

            }else{
                $data = array('login_fail' => true);
                //load login view
                $this->load->view('vw_home', $data);
            }
        }else{
            redirect('home', 'refresh');
        }


    }

    public function forgot_password(){

    }

    public function logout(){
        $this->session->unset_userdata('login_user');
        redirect('login', 'refresh');
    }
}